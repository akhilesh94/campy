package dotty;

public class CFGNode {
	
	private String value;
	private int minValue;
	private int maxValue;

	private CFGNode left;
	private CFGNode right;
	private CFGNode parent;

	public CFGNode(String value, int minValue, int maxValue) {
		this.value = value;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	public String getValue() { return this.value; }
	public void setValue(String value) { this.value = value; }
	public int getMinValue() { return this.minValue; }
	public int getMaxValue() { return this.maxValue; }	 
	public void setLeft(CFGNode left) { this.left = left; }
	public CFGNode getLeft() { return this.left; }	
	public void setRight(CFGNode right) { this.right = right; }
	public CFGNode getRight() { return this.right; }	
	public void setParent(CFGNode parent) { this.parent = parent; }
	public CFGNode getParent() { return this.parent; }	

	public String toString() {
		String l = null;
		String r = null;
		if(left != null)
			l = left.getValue();
		if(right != null)
			r = right.getValue();
		return value + "[" + minValue + "," + maxValue + "]" + " *** " + l + "--" + r;
	}
}
