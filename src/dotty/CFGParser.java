package dotty; 

import java.util.LinkedList;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.LexerInterpreter;
import org.antlr.v4.runtime.ParserInterpreter;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.tool.Grammar;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.InterpolationContext;

import intervalTree.IntervalTree;

import safetyChecker.LogUtils;

public class CFGParser {
	public static void main(String[] args) { 
		String input = "nn1^22*log(a)*log(b)";
		input = "i0 * i1";
		CFGParser p = new CFGParser(new InterpolationContext());
		p.parseInput(5, input, new LinkedList<String>()); 
	}

	private InterpolationContext ictx = null;
	CFGTree cfg = null;

	public CFGParser(InterpolationContext ictx) {
		this.ictx = ictx;
		this.cfg = new CFGTree(ictx);
	}

	public BoolExpr parseInput(int countNo, String input, LinkedList<String> varBounds) {
//		input = "nn1^2*log(a)*log(b)";
//		input = "i0 ^ 2";
//		input = "log(i1) * i1";
//		input = "a*(b^2+3)";
//		input = "a+b^2*c";
		final Grammar g = Grammar.load("calculator.g");
		LexerInterpreter lexEngine = g.createLexerInterpreter(new ANTLRInputStream(input));
		CommonTokenStream tokens = new CommonTokenStream(lexEngine);
		ParserInterpreter parser = g.createParserInterpreter(tokens);
		ParseTree t = parser.parse(g.getRule("expression").index);

		printParseTree(t, 0, parser);
		cfg.parseVarBounds(varBounds);
		BoolExpr bound = cfg.printCFG(countNo);

//		System.out.println("------------------------------------");
//		System.out.println(it.getIntervals(0,0).get(0).getStart());
		return bound;
	}


	 IntervalTree<String> it = new IntervalTree<String>();
	 int kk = 0;
	LinkedList<String> ll = new LinkedList<String>();
	void printParseTree(ParseTree t, int indent, ParserInterpreter p) {
		for(int i = 0; i < t.getChildCount(); i ++) {
			ParseTree pt = t.getChild(i);
			if(pt.getChildCount() == 0) {
				//for(int j = 0; j < indent; j++) System.out.print("  ");
				String si = t.getSourceInterval().toString();
				int start = Integer.valueOf(si.replaceAll("^(\\d+)\\.\\.(\\d+)$", "$1"));
				int end = Integer.valueOf(si.replaceAll("^(\\d+)\\.\\.(\\d+)$", "$2"));

				// add node
				CFGNode node = new CFGNode(t.getChild(i).getText(), start, end);
//				if(node.getValue().contains(")"))
//					System.out.println("SKIP:");
//				else
					cfg.addNode(node);
//				cfg.printCFG();

				//printType(t.getChild(i).toString());
//				System.out.println("ADDED" + t.getChild(i).getText()  + "-" + start + ".." + end + "-");
				it.addInterval(start,end,t.getChild(i).toString());
			} else 
				printParseTree(pt, indent+1, p);
			kk++;
		}
	}
}
