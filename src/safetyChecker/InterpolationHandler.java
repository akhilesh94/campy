package safetyChecker;

import com.microsoft.z3.*;
import com.microsoft.z3.InterpolationContext.ComputeInterpolantResult;
import com.microsoft.z3.enumerations.Z3_lbool;
import dotty.CFGParser;
import soot.jimple.IfStmt;

import java.util.LinkedList;
import java.util.Map;

public class InterpolationHandler {
    private static InterpolationContext ictx;

    private static InterpolationHandler itpHandler;
    private static Z3ScriptHandler z3Handler;
    private static CoverRelation coverRelation;

    static int i = 0;
    static int j = 0;

    private boolean isFeasible = true;

    Expr[] from = null;
    Expr[] to = null;

    public static InterpolationHandler getItpHandler() {
        if (itpHandler == null) {
            itpHandler = new InterpolationHandler(ictx, z3Handler, coverRelation);
        }
        return itpHandler;
    }

    public InterpolationHandler(InterpolationContext ictx, Z3ScriptHandler z3Handler, CoverRelation coverRelation) {
        this.ictx = ictx;
        this.z3Handler = z3Handler;
        this.coverRelation = coverRelation;
    }

    public InterpolationContext getInterpolationContext() {
        return this.ictx;
    }

    public BoolExpr buildTillV(Vertex errorRoot, Vertex v) {
        int i = 0;

        z3Handler.convertPathtoZ3Script(errorRoot, true);
        BoolExpr buildPat = ictx.mkTrue();
        BoolExpr conjunction = ictx.mkTrue();

        buildPat = ictx.mkAnd(buildPat, errorRoot.getOutgoingEdge().getZ3Expr());
        buildPat = ictx.MkInterpolant(buildPat);

        Vertex curV = errorRoot.getNextVertex();

        conjunction = ictx.mkEq(ictx.mkIntConst("C" + i), ictx.mkInt(i));
        buildPat = ictx.MkInterpolant(ictx.mkAnd(buildPat, conjunction));
        BoolExpr costInvariant = ictx.mkTrue();

        while (curV != v.getNextVertex()) {
            Edge edge = curV.getOutgoingEdge();
            if (edge.isErrorEdge())
                break;


            IntExpr x1 = ictx.mkIntConst("C" + (i + 1));
            IntExpr x2 = ictx.mkIntConst("C" + i);
            costInvariant = ictx.mkEq(
                    x1,
                    ictx.mkAdd(x2, ictx.mkInt(1)));

            if (edge.getUnit() instanceof IfStmt) {
                i++;
            }

            BoolExpr z3Expr = edge.getZ3Expr();
            conjunction = ictx.mkAnd(z3Expr, costInvariant, buildPat);
            edge.setBound(costInvariant);

            if (edge.isControlLocation()) buildPat = ictx.MkInterpolant(conjunction);
            else buildPat = conjunction;

            curV = curV.getNextVertex();
        }

        return buildPat;
    }

    public BoolExpr getMinimalSubtreeWithFalse(Vertex errorRoot, Model m) {
        BoolExpr pat = ictx.mkTrue();
        BoolExpr patNew = ictx.mkTrue();
        BoolExpr conjunction = ictx.mkTrue();
        int i = 0;

        pat = ictx.mkAnd(pat, errorRoot.getOutgoingEdge().getZ3Expr());
        pat = ictx.MkInterpolant(pat);

        Vertex currentVertex = errorRoot.getNextVertex();

        conjunction = ictx.mkEq(ictx.mkIntConst("C" + i), ictx.mkInt(i));
        pat = ictx.MkInterpolant(ictx.mkAnd(pat, conjunction));
        BoolExpr costInvariant = ictx.mkTrue();

        while (currentVertex != null) {
            Edge edge = currentVertex.getOutgoingEdge();
            if (edge.isErrorEdge())
                break;

            IntExpr x1 = ictx.mkIntConst("C" + (i + 1));
            IntExpr x2 = ictx.mkIntConst("C" + i);
            costInvariant = ictx.mkEq(
                    x1,
                    ictx.mkAdd(x2, ictx.mkInt(1)));
            if (edge.getUnit() instanceof IfStmt) {
                i++;
            }

            BoolExpr z3Expr = edge.getZ3Expr();
            conjunction = ictx.mkAnd(z3Expr, costInvariant, pat);
            edge.setBound(costInvariant);

            if (edge.isControlLocation()) pat = ictx.MkInterpolant(conjunction);
            else pat = conjunction;

            if (m.eval(pat, true).equals(ictx.mkFalse())) {
                patNew = buildTillV(errorRoot, currentVertex);
                return patNew;
            }
            currentVertex = currentVertex.getNextVertex();
        }

        return patNew;
    }

    public BoolExpr getBoundTree(Vertex errorRoot) {
        BoolExpr pat = ictx.mkTrue();
        BoolExpr conjunction = ictx.mkTrue();

        pat = ictx.mkAnd(pat, errorRoot.getOutgoingEdge().getZ3Expr());
        pat = ictx.MkInterpolant(pat);

        Vertex currentVertex = errorRoot.getNextVertex();

        conjunction = ictx.mkEq(ictx.mkIntConst("C" + i), ictx.mkInt(i));
        pat = ictx.MkInterpolant(ictx.mkAnd(pat, conjunction));
        BoolExpr costInvariant = ictx.mkTrue();

        while (currentVertex != null) {
            Edge edge = currentVertex.getOutgoingEdge();
            if (edge.isErrorEdge())
                break;

            if (edge.getUnit() instanceof IfStmt) {

                IntExpr x1 = ictx.mkIntConst("C" + (i + 1));
                IntExpr x2 = ictx.mkIntConst("C" + i);
                costInvariant = ictx.mkEq(
                        x1,
                        ictx.mkAdd(x2, ictx.mkInt(1)));

                if (edge.getUnit() instanceof IfStmt) {
                    i++;
                }

                BoolExpr z3Expr = edge.getZ3Expr();
                conjunction = ictx.mkAnd(z3Expr, costInvariant, pat);
                edge.setBound(costInvariant);
            }
            if (edge.isControlLocation()) pat = this.ictx.MkInterpolant(conjunction);
            else pat = conjunction;

            currentVertex = currentVertex.getNextVertex();
        }

        return pat;
    }

    private BoolExpr bound = null;

    public boolean createInterpolant(Vertex errorRoot, String isVerbose, String inputBound, LinkedList<String> varBounds) {
        ComputeInterpolantResult interpolantResult = null;
        boolean result;
        BoolExpr pathFormula = ictx.mkTrue();

        generateNameMapping();
        Model m = null;
        do {
            j++;

            z3Handler.convertPathtoZ3Script(errorRoot, true);

            pathFormula = ictx.mkAnd(pathFormula, getBoundTree(errorRoot));
            CFGParser parser = new CFGParser(ictx);
            bound = parser.parseInput(i, inputBound, varBounds);
            int prevNodeCounter = i;

            pathFormula = ictx.mkAnd(
                    pathFormula,
                    ictx.mkNot(bound));

            pathFormula = ictx.MkInterpolant(pathFormula);

            Params params = ictx.mkParams();

            BoolExpr simplifiedPathFormula = (BoolExpr) pathFormula.simplify();
            interpolantResult = ictx.ComputeInterpolant(simplifiedPathFormula, params);

            BoolExpr[] invariantList = interpolantResult.interp;

            updateInvariant(errorRoot, invariantList, isFeasible);

            Z3_lbool status = interpolantResult.status;

            if (status == Z3_lbool.Z3_L_FALSE) {
                result = false;
                break;
            } else if (status == Z3_lbool.Z3_L_TRUE) {
                result = true;
                m = interpolantResult.model;
                if (Utility.validateModel(m)) break;
                else {
                    BoolExpr origPathFormula = pathFormula;
                    z3Handler.convertPathtoZ3Script(errorRoot, false);
                    pathFormula = ictx.mkAnd(getBoundTree(errorRoot), bound);

                    if (m.eval(pathFormula, true).equals(ictx.mkTrue())) break;
                    else {
                        BoolExpr constraint1 = getMinimalSubtreeWithFalse(errorRoot, m);
                        BoolExpr constraint2 = ictx.mkAnd(constraint1, Utility.addTEqualsVT(m));
                        pathFormula = ictx.mkOr(pathFormula, Utility.addInputAxiomsSubTree(pathFormula, m, prevNodeCounter));

                        z3Handler.convertPathtoZ3Script(errorRoot, true);

                        pathFormula = ictx.mkAnd(pathFormula, origPathFormula, Utility.addBrokenAxiomsSubTree(pathFormula, m, constraint1, constraint2, true, i));
                    }
                }
            }
        } while (true);

        return result;
    }

    private void updateInvariant(Vertex errorRootVertex, BoolExpr[] invariantList, boolean isFeasible) {
        if (invariantList != null) {
            Vertex vertex = errorRootVertex;
            int index = 0;
            BoolExpr falseExpr = this.ictx.mkFalse();

            while (vertex != null && vertex.getOutgoingEdge() != null) {
                if (vertex.getOutgoingEdge().isControlLocation()) {
                    BoolExpr currentInvariant = vertex.getInvariant();
                    BoolExpr z3Invariant = null;
                    if (index >= invariantList.length && !isFeasible)
                        z3Invariant = falseExpr;
                    else if (index >= invariantList.length && isFeasible) {
                        z3Invariant = ictx.mkTrue();
                    } else
                        z3Invariant = invariantList[index];
                    BoolExpr newInvariant = (BoolExpr) z3Invariant.substitute(this.from, this.to);

                    if (vertex.getInvariant() == null)
                        vertex.setInvariant(newInvariant);
                    else {
                        BoolExpr disjunction = this.ictx.mkOr(newInvariant, currentInvariant);
                        BoolExpr simplified = (BoolExpr) disjunction.simplify();
                        vertex.setInvariant(simplified);
                        coverRelation.checkHoldsAndClearCoverRelation(vertex);
                    }
                    index++;
                }

                vertex = vertex.getNextVertex();
            }
        } else {
            Vertex vertex = errorRootVertex;
            while (vertex != null) {
                if (vertex.isReturnLocation()) break;

                if (vertex.getOutgoingEdge().isControlLocation()) {
                    BoolExpr trueExpr = this.ictx.mkTrue();
                    BoolExpr currentInvariant = vertex.getInvariant();
                    if (currentInvariant == null)
                        vertex.setInvariant(trueExpr);
                    else {
                        BoolExpr disjunction = this.ictx.mkOr(trueExpr, currentInvariant);
                        vertex.setInvariant(disjunction);
                    }
                }
                vertex = vertex.getNextVertex();
            }
        }
    }

    private void generateNameMapping() {
        Map<String, String> substitute = this.z3Handler.getSubstitute();
        Map<String, Sort> substituteSort = this.z3Handler.getSubstituteSort();
        this.from = new Expr[substitute.size()];
        this.to = new Expr[substitute.size()];

        int i = 0;
        for (Map.Entry<String, String> entry : substitute.entrySet()) {
            String fromStr = entry.getKey();
            String toStr = entry.getValue();

            Sort sort = substituteSort.get(fromStr);
            if (sort == null) {
                sort = this.ictx.mkIntSort();
            }

            from[i] = this.ictx.mkConst(fromStr, sort);
            to[i] = this.ictx.mkConst(toStr, sort);
            i++;
        }
    }
}
