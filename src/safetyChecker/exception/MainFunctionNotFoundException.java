package safetyChecker.exception;

public class MainFunctionNotFoundException extends Exception {

	public static final long serialVersionUID = 1L;
	
	public MainFunctionNotFoundException(String message) {
		super(message);
	}

	public String getMessage() {
		return super.getMessage();
	}
}	

