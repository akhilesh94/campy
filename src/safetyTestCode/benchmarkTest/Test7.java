package safetyTestCode.benchmarkTest;

import java.io.*;
import java.util.*;
 
import safetyChecker.ErrorLable;

public class Test7 {    
  public static void main(int count, int num) throws java.io.IOException {
    java.io.BufferedReader br = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
    int i = 0, tmp=0,fc;
    
    StringBuilder sb = new StringBuilder();
    
    // TO find factorial
    while(count-->0){
    
        int[] fact = new int[200];
        fc=fact[0]=1;
        while(num >1){
            for(i=0;tmp != 0 || i<fc;i++){
                tmp += fact[i] * num;
                fact[i] = tmp%10;
                tmp /= 10;
            }
            num--;
            fc=i;               
        }
        for(fc--;fc>=0;fc--){
            sb.append(fact[fc]);
        }
        sb.append("\n");
    }
    ErrorLable.Error();
    System.out.print(sb.toString());
}
}
