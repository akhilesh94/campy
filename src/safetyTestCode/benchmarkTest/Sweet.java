package safetyTestCode.benchmarkTest;

import java.io.*;
import java.util.*;
import java.math.*;
 
import safetyChecker.ErrorLable;

class Sweet {
    static BufferedReader in;
    static PrintWriter out;
    static StringTokenizer tok; 
    
    
    static void test(int t, int n) {
        int a1 = 1;
        int b = 2;
        int c = 3;
        while(t-->0)
        {
        
            int x=100;
            int total=0;
            int min=Integer.MAX_VALUE;
            for(int i=0;i<n;i++)
            {
                int a=100;
                total=total+a;
                if(a<min)
                    min=a;
            }
            int all=total/x;
            int all_min=(total-min)/x;
            if(all_min==all)
            {
                out.println("-1");
            }
            else
            {
                out.println(all);
            }
        }
        ErrorLable.Error();
        //out.println(res);
        
    }
    
    static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }
    
    static long sqr(long x) {
        return x * x;
    }
    
    static int nextInt() {
        return Integer.parseInt(next());
    }
 
    static long nextLong() {
        return Long.parseLong(next());
    }
 
    static double nextDouble() {
        return Double.parseDouble(next());
    }
 
    static BigInteger nextBigInteger() {
        return new BigInteger(next());
    }
    
    static String next() {
        while (tok == null || !tok.hasMoreTokens()) {
            try {
                tok = new StringTokenizer(in.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return tok.nextToken();
    }
    
    static String nextLine() {
        try {
            return in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
 
    static boolean hasNext() {
        while (tok == null || !tok.hasMoreTokens()) {
            String s = null;
            try {
                s = in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (s == null) {
                return false;
            } else {
                tok = new StringTokenizer(s);
            }
        }
        return true;
    }
    
    public static void main(String args[]) {
        try {
            in = new BufferedReader(new InputStreamReader(System.in));
            out = new PrintWriter(new OutputStreamWriter(System.out));
            //in = new BufferedReader(new FileReader("input.in"));
            //out = new PrintWriter(new FileWriter("output.out"));
            // solve();
            out.close();
        } catch (Throwable e) {
            e.printStackTrace();
            java.lang.System.exit(1);
        }
    }
} 
