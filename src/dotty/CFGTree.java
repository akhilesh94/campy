package dotty;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.FuncDecl;
import com.microsoft.z3.IntExpr;
import com.microsoft.z3.IntNum;
import com.microsoft.z3.InterpolationContext;
import com.microsoft.z3.Sort;

import safetyChecker.LogUtils;

public class CFGTree {

	CFGNode root;
	HashSet<String> varMap = new HashSet<String>();
	InterpolationContext ictx = null;
	Sort iSort = null; //ictx.mkIntSort();
	Sort[] domainInt = null; //{ iSort, iSort };
	FuncDecl myMul = null; //ictx.mkFuncDecl("MUL", domainInt, iSort);
	FuncDecl myExp = null; //ictx.mkFuncDecl("EXP", domainInt, iSort);
	FuncDecl myLog = null; //ictx.mkFuncDecl("LOG", domainInt, iSort);
	private IntNum ZERO = null;// this.ictx.mkInt(0);
	private String MUL = "*";
	private String ADD = "+";
	private String MIN = "-";
	private String EXP = "^";
	private String LOG = "log";
	private String PARANTHESIS = "()";
	private BoolExpr parameterBound = null;
	private ArrayList<BoolExpr> parameters = null;

	public CFGTree(InterpolationContext ictx) {
		this.ictx = ictx;
		this.iSort = ictx.mkIntSort();
		this.domainInt = new Sort[]{ iSort, iSort };
		this.myMul = ictx.mkFuncDecl("MUL", domainInt, iSort);
		this.myExp = ictx.mkFuncDecl("EXP", domainInt, iSort);
		this.myLog = ictx.mkFuncDecl("LOG", domainInt, iSort);
		this.ZERO = ictx.mkInt(0);
	}

	public void addNode(CFGNode node) {
		if (this.root == null)
			this.root = node;
		else
			searchInternalAndInsert(node, root);
		logln(node.toString());
	}

	public void searchInternalAndInsert(CFGNode newNode, CFGNode node) {
		if (this.isCovering(newNode, node)) {
			logln("***Covering" + newNode + "***" + node);
			addAsParent(newNode, node);
		} else if (this.isInside(newNode, node)) {
			logln("Inside" + newNode);
			if (newNode.getMinValue() == node.getMinValue()) {
				logln("ADD LEFT" + newNode);
				if (node.getLeft() == null)
					addAsLeftChild(newNode, node);
				else
					searchInternalAndInsert(newNode, node.getLeft());
			} else {
				logln(">>>>" + newNode);
				if (node.getRight() == null)
					addAsRightChild(newNode, node);
				else
					searchInternalAndInsert(newNode, node.getRight());
				logln("<<<<" + node);
			}
		}
	}

	public void addAsParent(CFGNode newNode, CFGNode node) {
		if (node.getParent() == null) {
			if (newNode.getValue().equals(")")) {
				if (!node.getValue().equals("("))
					throw new RuntimeException("Handle PARANTHESIS");
				else if (node.getMinValue() == newNode.getMinValue() && node.getMaxValue() == newNode.getMaxValue()) {
					node.setValue(node.getValue() + newNode.getValue());
				} else
					throw new RuntimeException("PARANTHESIS does not match");

			} else {
				this.root = newNode;
				newNode.setLeft(node);
				node.setParent(newNode);
			}
		} else {
//			System.out.println(node.getValue() + "~~~" + newNode.getValue());
//			System.out.println(node + "~~~~" + newNode);
			if (node.getMinValue() == newNode.getMinValue() && node.getMaxValue() == newNode.getMaxValue()) {
				node.setValue(node.getValue() + newNode.getValue());
			} else {
				CFGNode parent = node.getParent();
				if (parent.getLeft() == node)
					parent.setLeft(newNode);
				else
					parent.setRight(newNode);

				if (newNode.getMinValue() == node.getMinValue())
					newNode.setLeft(node);
				else
					newNode.setRight(node);

				node.setParent(newNode);
				newNode.setParent(parent);
			}
		}

	}

	public void addAsLeftChild(CFGNode newNode, CFGNode node) {
		newNode.setParent(node);
		if (node.getLeft() == null)
			node.setLeft(newNode);
		else {
			CFGNode n = node.getLeft();
			node.setLeft(newNode);
			n.setParent(newNode);
			if (newNode.getMinValue() == n.getMinValue())
				newNode.setLeft(n);
			else
				newNode.setRight(n);
		}
	}

	public void addAsRightChild(CFGNode newNode, CFGNode node) {
		newNode.setParent(node);
		if (node.getRight() == null)
			node.setRight(newNode);
		else {
			CFGNode n = node.getRight();
			node.setRight(newNode);
			n.setParent(newNode);
			if (newNode.getMinValue() == n.getMinValue())
				newNode.setLeft(n);
			else
				newNode.setRight(n);
		}
	}

	public boolean isInside(CFGNode n1, CFGNode n2) {
		if (n1.getMinValue() >= n2.getMinValue() && n1.getMaxValue() <= n2.getMaxValue())
			return true;
		return false;
	}

	public boolean isCovering(CFGNode n1, CFGNode n2) {
		if (n1.getMinValue() <= n2.getMinValue() && n1.getMaxValue() >= n2.getMaxValue())
			return true;
		return false;
	}

	public BoolExpr printCFG(int i) {
		parameterBound = null;
		ArithExpr ae = printSubTree(root, 0);
		if(parameters.size() == 1)
			parameterBound = parameters.get(0);
		else if (parameters.size() > 1)
			parameterBound = this.ictx.mkAnd(parameters.toArray(new BoolExpr[parameters.size()]));

		IntExpr Ci = this.ictx.mkIntConst("C" + i);
		BoolExpr CiLtAe = this.ictx.mkLt(Ci, ae);

		//System.out.println(this.ictx.mkImplies(parameterBound, CiLtAe));
		if(parameterBound == null)
			return CiLtAe;
		return this.ictx.mkImplies(parameterBound, CiLtAe);
	}


	private ArithExpr printSubTree(CFGNode node, int ind) {
		if (node == null)
			return null;

		ArithExpr leftAE = printSubTree(node.getLeft(), ind + 1);
		ArithExpr rightAE = printSubTree(node.getRight(), ind + 1);

		// for(int i = 0; i < ind; i++)
		// System.out.print(" ");
		// System.out.println(node.getValue());
		ArithExpr ae = null;
		if (node.getValue().equals(MUL)) {
			ae = (ArithExpr) this.myMul.apply(leftAE, rightAE);
		} else if (node.getValue().equals(EXP)) {
			if (node.getRight().getValue().matches("^[0-9]+$")) {
				int exp = Integer.parseInt(node.getRight().getValue());
				ArithExpr[] aeArray = new ArithExpr[exp];
				for (int i = 0; i < exp; i++) {
					aeArray[i] = leftAE;
				}
				ae = (ArithExpr) this.myMul.apply(aeArray);
			} else
				ae = (ArithExpr) this.myExp.apply(leftAE, rightAE);
		} else if (node.getValue().equals(PARANTHESIS)) {
			if (node.getLeft() == null) {
				ae = rightAE;
			} else if (node.getLeft().getValue().equals(LOG))
				ae = (ArithExpr) this.myLog.apply(rightAE, this.ictx.mkInt(2));
			else
				throw new RuntimeException("Left is not log, solve this condition");
		} else if (node.getValue().equals(ADD)) {
			ae = this.ictx.mkAdd(leftAE, rightAE);
		} else if (node.getValue().equals(MIN)) {
			ae = this.ictx.mkSub(leftAE, rightAE);
		} else if (node.getValue().equals(LOG)) {
			// do nothing in here
		} else {
			if (node.getValue().matches("^[0-9]+$")) {
				ae = this.ictx.mkInt(Integer.parseInt(node.getValue()));
			} else {
				ae = this.ictx.mkIntConst(node.getValue());
				if(!varMap.contains(node.getValue())) {
					parameters.add(this.ictx.mkGt(ae, ZERO));
					varMap.add(node.getValue());
				}
			}
		}
		return ae;

	}

	public void parseVarBounds(LinkedList<String> varBounds) {
		parameters = new ArrayList<BoolExpr>();
		for(String varBound : varBounds) {
			String splitStr = null;
			if(varBound.contains(">=")) 
				splitStr = ">=";
			else if(varBound.contains(">")) 
				splitStr = ">";
			else if(varBound.contains("<=")) 
				splitStr = "<=";
			else if(varBound.contains("<"))
				splitStr = "<";
			String[] ar = varBound.split(splitStr);
			String leftStr = ar[0].trim();
			String rightStr = ar[1].trim();

			if(rightStr.matches("^[0-9]+$")) {
				ArithExpr rExpr = this.ictx.mkInt(Integer.parseInt(rightStr));
				ArithExpr lExpr = this.ictx.mkIntConst(leftStr);
				if(splitStr.equals(">=") || splitStr.equals(">"))  {
					parameters.add(this.ictx.mkGt(lExpr, rExpr));
					varMap.add(leftStr);
				} else 
					parameters.add(this.ictx.mkLt(lExpr, rExpr));
			} else {
				ArithExpr rExpr = this.ictx.mkIntConst(rightStr);
				ArithExpr lExpr = this.ictx.mkInt(Integer.parseInt(leftStr));
				if(splitStr.equals(">=") || splitStr.equals(">"))  
					parameters.add(this.ictx.mkGt(lExpr, rExpr));
				else {
					parameters.add(this.ictx.mkLt(lExpr, rExpr));
					varMap.add(rightStr);
				}

			}


		}

	}

	boolean print = false;

	private void log(String l) {
		if (print)
			System.out.print(l);
	}

	private void logln(String l) {
		if (print)
			System.out.println(l);
	}
}
