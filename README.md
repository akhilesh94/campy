Please refer to our technical publication, "Complexity Verification Using Guided Theorem Enumeration", published in the 44th ACM SIGPLAN Symposium on Principles of Programming Languages, to understand more about this project.

In order to build the project, modify the path to jdk in `build.properties` and then run:
`> ant`.

In order to run the examples, run:
`> ./run_bench.sh`

Please refer to LICENSE.txt for licensing information