package safetyTestCode.benchmarkTest;

import java.util.Scanner;

import safetyChecker.ErrorLable;

class BirthDayCandles {
  public static void test(int n)
  {
    Scanner sc = new Scanner(System.in);
    int t = sc.nextInt();
    while(t-->0)
    {
      int[] arr = new int[n];
      for(int i=0;i<n;i++)
      {
        arr[i] = sc.nextInt();
      }

      int min = 10;
      int index = 0;
      for(int i=0;i<n;i++)
      {
        if(arr[i]<min)
        {
          min = arr[i];
          index = i;
        }
        else if(arr[i]==min && index==0)
          index = i;
      }
      if(index==0)
        System.out.print(1);
      for(int i=0;i<=min;i++)
      {
        System.out.print(index);
      }
      System.out.println();
    }
    ErrorLable.Error();
  }
}
