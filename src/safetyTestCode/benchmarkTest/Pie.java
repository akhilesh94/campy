package safetyTestCode.benchmarkTest;


import safetyChecker.ErrorLable;

import java.util.Arrays;
import java.util.Scanner;

public class Pie {
    public static int numPlaced(int[] pies, int[] racks) {
        Arrays.sort(pies);
        Arrays.sort(racks);
        int ip=0, ir=0;
        for(; ir<racks.length; ir++) {
            if(pies[ip] <= racks[ir]) ip++;
            else continue;
        }
        return ip++;
    }

    public static void main(int t, int N) {
        Scanner in = new Scanner(System.in);

        for(int i=0; i<t; i++) {
            int[] pies = new int[N];
            int[] racks = new int[N];
            for(int j=0; j<N; j++)
                pies[j] = in.nextInt();
            for(int j=0; j<N; j++)
                racks[j] = in.nextInt();
            // System.out.println(numPlaced(pies, racks));
        }
        in.close();
        ErrorLable.Error();
    }
}
