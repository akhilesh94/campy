package safetyTestCode.benchmarkTest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import safetyChecker.ErrorLable;

class Scroll {
    public static void solve(int t, int n, int a, int b) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int i, j, k;
        int p;

        for (i = 0; i < t; i++) {
            if (a < b) {
                p = a;
                a = b;
                b = p;
            }

            j = 0;
            while (a > 0) {
                if (a % 2 == 1)
                    j++;
                a = a / 2;
            }

            k = 0;
            while (b > 0) {
                if (b % 2 == 1)
                    k++;
                b = b / 2;
            }

            p = 0;
            if (j > n - k)
                p += n - k;
            else
                p += j;

            if (k > n - j)
                p += n - j;
            else
                p += k;

            if (j + k == n) ;
                // System.out.println((int)(Math.pow(2,n)-1));
            else if (j + k < n) ;
                // System.out.println((int)(Math.pow(2,n)-Math.pow(2,n-(j+k))));
            else ;
            // System.out.println((int)(Math.pow(2,n)-Math.pow(2,j+k-n)));
        }

        ErrorLable.Error();

    }
}

