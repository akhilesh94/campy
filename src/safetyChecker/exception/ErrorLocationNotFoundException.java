package safetyChecker.exception;

public class ErrorLocationNotFoundException extends Exception {
	
	public static final long serialVersionUID = 1L;

	public ErrorLocationNotFoundException(String message) {
		super(message);
	}

	public String getMessage() {
		return super.getMessage();
	}
}	
