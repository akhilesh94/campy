package safetyTestCode.benchmarkTest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import safetyChecker.ErrorLable;
class JewelThief
{
    public static void main(int t)throws java.io.IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        PrintWriter outt=new PrintWriter(System.out);
        
        int n,i,sum;
        String j,s;
        while(t-->0)
        {
            sum=0;
            j=br.readLine();
            s=br.readLine();
            int a[]=new int[150];
            int b[]=new int[150];
            for(i=0;i<=200;i++)
            {
                if(i<j.length())    a[j.charAt(i)]++;
                if(i<s.length())    b[s.charAt(i)]++;
                if(i>=s.length() && i>=j.length())  break;
            }
            for(i=0;i<150;i++)
                if(a[i]!=0 && b[i]!=0)
                    sum+=b[i];
            outt.println(sum);
        }
        
        outt.flush();
     
        ErrorLable.Error();
    }
}
