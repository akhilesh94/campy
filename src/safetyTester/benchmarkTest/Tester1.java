package safetyTester.benchmarkTest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import safetyChecker.LogUtils;
import safetyChecker.ProgramTree;

import soot.Body;
import soot.BodyTransformer;
import soot.G;
import soot.PackManager;
import soot.SootMethod;
import soot.Transform;
import soot.options.Options;

public class Tester1 {
	static Map<String, Body> stores = new HashMap<String, Body>();

	private static long startTime = System.currentTimeMillis();
	public static void main(String[] args) throws FileNotFoundException,
			UnsupportedEncodingException {
		String isVerbose = args[0];
		String testClass = args[1];
		String bound = null;
		LinkedList<String> varBounds = new LinkedList<String>();
		if(args.length > 2) {
			boolean flag = false;
			for(int i = 2; i < args.length; i++) {
				if(args[i].equalsIgnoreCase("--bound")) {
					i++;
					bound = args[i];
				} else if(args[i].equalsIgnoreCase("--varbound")) { 
					flag = true;
					i++;
				}
				if(flag) varBounds.add(args[i]);
			}
		}
		Options.v().set_src_prec(Options.src_prec_c);
		Options.v().set_output_format(Options.output_format_shimple);
		Options.v().set_allow_phantom_refs(true);
		G.v().out = new PrintStream(new File("./soot.out"));
		String[] sootArgs = new String[] {
				"-process-dir",
				"./bin",
				"-output-dir", "./src/output/safetyTest" };
		PackManager.v().getPack("stp")
				.add(new Transform("stp.test", new BodyTransformer() {

					@Override
					protected void internalTransform(Body body,
							String phaseName, Map<String, String> options) {
						// hack here
						SootMethod method = body.getMethod();
						String methodSig = method.getSignature();
						
						stores.put(methodSig, body);
					}
				}));
		soot.Main.main(sootArgs);

		String mainFunction = null;
		if (testClass.equals("Array2")) mainFunction = "<safetyTestCode.benchmarkTest.Array2: void test(int)>";
		else if (testClass.equals("BirthDayCandles")) mainFunction = "<safetyTestCode.benchmarkTest.BirthDayCandles: void test(int)>";
		else if (testClass.equals("Bit")) mainFunction = "<safetyTestCode.benchmarkTest.Bit: void test(int)>";
		else if (testClass.equals("CodeChefJava")) mainFunction = "<safetyTestCode.benchmarkTest.CodeChefJava: void test(java.lang.String,int,int)>";
		else if (testClass.equals("DRGNBOOL")) mainFunction = "<safetyTestCode.benchmarkTest.DRGNBOOL: void test(int,int,int)>";
		else if (testClass.equals("Fibo")) mainFunction = "<safetyTestCode.benchmarkTest.Fibo: void test(int,int,int)>";
		else if (testClass.equals("Ideone")) mainFunction = "<safetyTestCode.benchmarkTest.Ideone: void test(int,int)>";
		else if (testClass.equals("Jewel_and_Stone")) mainFunction = "<safetyTestCode.benchmarkTest.Jewel_and_Stone: void test(int,int,int)>";
		else if (testClass.equals("JewelThief")) mainFunction = "<safetyTestCode.benchmarkTest.JewelThief: void main(int)>";
		else if (testClass.equals("Loops_1")) mainFunction = "<safetyTestCode.benchmarkTest.Loops_1: void test(int,int)>";
		else if (testClass.equals("Sweet")) mainFunction = "<safetyTestCode.benchmarkTest.Sweet: void test(int,int)>";
		else if (testClass.equals("Test5")) mainFunction = "<safetyTestCode.benchmarkTest.Test5: void main(int,int)>";
		else if (testClass.equals("Scroll")) mainFunction = "<safetyTestCode.benchmarkTest.Scroll: void solve(int,int,int,int)>";
		else if (testClass.equals("LIS")) mainFunction = "<safetyTestCode.benchmarkTest.LIS: int lengthOfLIS(int[],int)>";
		else if (testClass.equals("Pie")) mainFunction = "<safetyTestCode.benchmarkTest.Pie: void main(int,int)>";
		else if (testClass.equals("SmartSieve")) mainFunction = "<safetyTestCode.benchmarkTest.SmartSieve: void SmartSieve(int,int)>";
		else if (testClass.equals("Test0")) mainFunction = "<safetyTestCode.benchmarkTest.Test0: int test1(int)>";
		else if (testClass.equals("Test1")) mainFunction = "<safetyTestCode.benchmarkTest.Test1: void main(int,int)>";
		else if (testClass.equals("Test3")) mainFunction = "<safetyTestCode.benchmarkTest.Test3: void test(int,int)>";
		else if (testClass.equals("Test2")) mainFunction = "<safetyTestCode.benchmarkTest.Test2: void main(int,int)>";
		else if (testClass.equals("Test6")) mainFunction = "<safetyTestCode.benchmarkTest.Test6: void solve(int,int)>";
	
		try {			
			ProgramTree pTree = new ProgramTree(stores, mainFunction, true, isVerbose, bound, varBounds);

		} catch (Exception exp){
			LogUtils.fatalln("*******************************************");
			LogUtils.fatalln("*********** EXCEPTION OCCURRED ************");
			LogUtils.fatalln("*******************************************");
			LogUtils.fatalln(exp);
			LogUtils.fatalln("******************"); 
		//	LogUtils.fatalln(exp.getStackTrace());
			LogUtils.fatalln("******************"); 
			exp.printStackTrace();
		}

	}
}
