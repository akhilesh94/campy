package safetyTestCode.benchmarkTest;

import java.util.*;
import safetyChecker.ErrorLable;
class Solution
{
    public static void test(int t, int n)
    {
        Scanner in=new Scanner(System.in);
        
        for(int i=1;i<=t;i++)
        {
            int m;            
            m=in.nextInt();
            int arr[]=new int[n];
            int j=0,k=0;
            while(k<n)
            {
                if(arr[j]==0)
                {
                    arr[j]=1;
                    j=(j+m)%(n);k++;
                }
                else 
                {
                    System.out.println("No "+k);
                    break;
                }
            }
            if(k==n)
                System.out.println("Yes");
        }
        ErrorLable.Error();
    }
}
