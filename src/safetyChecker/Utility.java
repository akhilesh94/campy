package safetyChecker;

import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Expr;
import com.microsoft.z3.FuncDecl;
import com.microsoft.z3.FuncInterp;
import com.microsoft.z3.IntExpr;
import com.microsoft.z3.InterpolationContext;
import com.microsoft.z3.Model;
import com.microsoft.z3.Sort;
import com.microsoft.z3.Symbol;
import com.microsoft.z3.Z3Exception;
import com.microsoft.z3.FuncInterp.Entry;

public class Utility {
    private static InterpolationContext ictx = InterpolationHandler.getItpHandler().getInterpolationContext();

    public static Boolean isInModel(Model m, Expr e) {
        for (FuncDecl fi : m.getConstDecls()) {
            try {
                if (m.getConstInterp(fi).equals(e)) {
                    if (fi.getName().toString().startsWith("C")) continue;
                    return true;
                }
            } catch (Z3Exception exception) {
                continue;
            }
        }
        return false;
    }

    public static Symbol getSymbolName(Model m, Expr e) {
        Symbol foo = null;
        for (FuncDecl fi : m.getConstDecls()) {
            try {
                if (m.getConstInterp(fi).equals((e))) {
                    if (fi.getName().toString().startsWith("C")) continue;
                    return fi.getName();
                }
            } catch (Z3Exception exception) {
                continue;
            }
        }
        return foo;
    }

    public static Boolean validateModel(Model m) {
        Expr resMul = ictx.mkInt(1);
        Expr resExp = ictx.mkInt(1);
        Expr resLog = ictx.mkInt(1);
        for (FuncDecl f : m.getFuncDecls()) {
            if (f.getName().toString().equals("MUL") || f.getName().toString().equals("EXP") || f.getName().toString().equals("LOG")) {
                FuncInterp interpretation = m.getFuncInterp(f);
                for (Entry x : interpretation.getEntries()) {
                    resMul = ictx.mkInt(1);
                    resExp = ictx.mkInt(1);
                    resLog = ictx.mkInt(1);
                    for (Expr xi : x.getArgs()) {
                        if (f.getName().toString().equals("MUL"))
                            resMul = ictx.mkMul((ArithExpr) resMul, (ArithExpr) xi.simplify());
                        else if (f.getName().toString().equals("EXP")) {
                            if (xi.equals(x.getArgs()[0]) && !(x.getArgs()[0].equals(x.getArgs()[1]))) {
                                continue;
                            } else {
                                if (x.getArgs()[1].simplify().equals(ictx.mkInt(0))) {
                                    resExp = ictx.mkInt(0);
                                    break;
                                } else {
                                    for (IntExpr i = ictx.mkInt(1); !(i.simplify().equals(ictx.mkAdd((ArithExpr) (x.getArgs()[1]).simplify(), ictx.mkInt(1)).simplify())); i = (IntExpr) ictx.mkAdd(i, ictx.mkInt(1)).simplify()) {
                                        resExp = ictx.mkMul((ArithExpr) resExp, (ArithExpr) x.getArgs()[0]);
                                    }

                                    break;
                                }
                            }
                        } else if (f.getName().toString().equals("LOG")) {
                            if (xi.equals(x.getArgs()[0])) continue;
                            else {
                                if (x.getArgs()[0].simplify().equals(ictx.mkInt(1))) {
                                    resExp = ictx.mkInt(0);
                                    break;
                                } else {
                                    for (IntExpr i = ictx.mkInt(2); !(i.simplify().equals((x.getArgs()[0]).simplify())); i = (IntExpr) ictx.mkMul(i, ictx.mkInt(2)).simplify()) {

                                        resLog = ictx.mkAdd((ArithExpr) resLog, (ArithExpr) ictx.mkInt(1)).simplify();
                                    }

                                }
                            }
                        } else ;
                    }

                    if (resMul.simplify().equals(x.getValue()) || resExp.simplify().equals(x.getValue()) || resLog.simplify().equals(x.getValue()))
                        continue;
                    else {
                        for (Expr xi : x.getArgs()) {
                            if (isInModel(m, xi)) return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    static BoolExpr addTEqualsVT(Model m) {
        BoolExpr pat = ictx.mkTrue();
        Expr resMul = ictx.mkInt(1);
        for (FuncDecl f : m.getFuncDecls()) {
            if (f.getName().toString().equals("MUL") || f.getName().toString().equals("EXP") || f.getName().toString().equals("LOG")) {
                FuncInterp interpretation = m.getFuncInterp(f);
                for (Entry x : interpretation.getEntries()) {
                    resMul = ictx.mkInt(1);
                    for (Expr xi : x.getArgs()) {
                        if (f.getName().toString().equals("MUL"))
                            resMul = ictx.mkMul((ArithExpr) resMul, (ArithExpr) xi.simplify());
                        else if (f.getName().toString().equals("EXP")) ;
                        else if (f.getName().toString().equals("LOG")) ;
                        else ;
                    }

                    if (resMul.simplify().equals(x.getValue())) continue;
                    else {
                        for (Expr xi : x.getArgs()) {
                            if (isInModel(m, xi))
                                pat = ictx.mkAnd(pat, ictx.mkEq(ictx.mkIntConst(getSymbolName(m, xi)), xi));
                        }
                    }
                }
            }
        }
        return pat;
    }

    public static BoolExpr addBrokenAxiomsSubTree(BoolExpr pathFormula, Model m, BoolExpr t1, BoolExpr b1, Boolean b, int nodeCounter) {
        InterpolationContext ictx = InterpolationHandler.getItpHandler().getInterpolationContext();

        BoolExpr pat = ictx.mkTrue();

        Expr resMul = ictx.mkInt(1);
        Expr resExp = ictx.mkInt(1);
        Expr resLog = ictx.mkInt(1);
        for (FuncDecl f : m.getFuncDecls()) {
            if (f.getName().toString().equals("MUL")) {
                FuncInterp interpretation = m.getFuncInterp(f);
                for (Entry x : interpretation.getEntries()) {
                    resMul = ictx.mkInt(1);
                    resExp = ictx.mkInt(1);
                    for (Expr xi : x.getArgs()) {
                        resMul = ictx.mkMul((ArithExpr) resMul, (ArithExpr) xi);
                    }

                    if (resMul.simplify().equals(x.getValue())) continue; // correct interpretation, move on
                    else {
                        for (Expr xi : x.getArgs()) {
                            if (isInModel(m, xi)) {
                                pat = ictx.mkAnd(pat, addAxiomsMULSubTree(pathFormula, m, xi, x, t1, b1, b));
                            }
                        }
                    }
                }
            }

            if (f.getName().toString().equals("EXP")) {
                FuncInterp interpretation = m.getFuncInterp(f);
                for (Entry x : interpretation.getEntries()) {

                    resExp = ictx.mkInt(1);
                    for (Expr xi : x.getArgs()) {
                        if (xi.equals(x.getArgs()[0])) continue;
                        else {
                            if (x.getArgs()[1].simplify().equals(ictx.mkInt(0))) {
                                resExp = ictx.mkInt(0);
                                break;
                            } else {
                                for (IntExpr i = ictx.mkInt(1); !(i.simplify().equals((x.getArgs()[1]).simplify())); i = (IntExpr) ictx.mkAdd(i, ictx.mkInt(1)).simplify()) {
                                    resExp = ictx.mkMul((ArithExpr) resExp, (ArithExpr) x.getArgs()[0]);
                                }
                            }
                        }
                    }

                    if (resExp.simplify().equals(x.getValue())) continue;
                    else {
                        for (Expr xi : x.getArgs()) {
                            if (isInModel(m, xi)) {
                                pat = ictx.mkAnd(pat, addAxiomsEXPSubTree(m, xi, x, t1, b1, b), addAxiomsMULSubTree(pathFormula, m, xi, x, t1, b1, b));
                                return pat;
                            }
                        }
                    }
                }
            }

            if (f.getName().toString().equals("LOG")) {
                FuncInterp interpretation = m.getFuncInterp(f);
                for (Entry x : interpretation.getEntries()) {
                    resExp = ictx.mkInt(1);
                    for (Expr xi : x.getArgs()) {
                        if (xi.equals(x.getArgs()[0])) continue;
                        else {
                            if (x.getArgs()[0].simplify().equals(ictx.mkInt(1))) {
                                resExp = ictx.mkInt(0);
                                break;
                            } else {
                                for (IntExpr i = ictx.mkInt(2); !(i.simplify().equals((x.getArgs()[0]).simplify())); i = (IntExpr) ictx.mkMul(i, ictx.mkInt(2)).simplify()) {

                                    resLog = ictx.mkAdd((ArithExpr) resLog, (ArithExpr) ictx.mkInt(1)).simplify();
                                }
                            }
                        }
                    }

                    if (resLog.simplify().equals(x.getValue())) continue;
                    else {
                        for (Expr xi : x.getArgs()) {
                            if (isInModel(m, xi)) {
                                pat = ictx.mkAnd(pat, addAxiomsLOGSubTree(m, xi, x, t1, b1, b), addAxiomsMULSubTree(pathFormula, m, xi, x, t1, b1, b));
                                return pat;
                            }
                        }
                    }
                }
            }
        }
        return pat;
    }

    public static BoolExpr addAxiomsLOGSubTree(Model m, Expr i, Entry x, BoolExpr t1, BoolExpr b1, Boolean b) {
        BoolExpr pat = ictx.mkTrue();
        IntExpr z = null;
        IntExpr z1 = null;

        for (FuncDecl c : m.getConstDecls()) {
            try {
                if (m.getConstInterp(c).equals(i)) {
                    if (c.getName().toString().startsWith("C")) continue;
                    pat = ictx.mkAnd(pat, Axiom.groundAxLog0(c.getName(), b));
                    pat = ictx.mkAnd(pat, Axiom.groundAxLog2(c.getName(), b));

                    IntExpr value = ictx.mkIntConst(c.getName());
                    IntExpr one = ictx.mkInt(1);
                    IntExpr zero = ictx.mkInt(0);

                    z = (IntExpr) i;

                    for (Expr yi : x.getArgs()) {
                        if (!(i.equals(yi))) z = (IntExpr) yi;
                    }

                    for (FuncDecl c1 : m.getConstDecls()) {
                        try {
                            if (m.getConstInterp(c1).equals(z)) {
                                if (c1.getName().toString().startsWith(("C"))) continue;
                                z1 = ictx.mkIntConst(c1.getName());
                            } else z1 = ictx.mkIntConst(c.getName());
                        } catch (Z3Exception e) {
                            continue;
                        }
                    }

                    Sort iSort = ictx.mkIntSort();
                    Sort[] domainInt = {iSort, iSort};
                    FuncDecl myLog = ictx.mkFuncDecl("LOG", domainInt, iSort);
                    BoolExpr boolVar = ictx.mkGt(z, ictx.mkInt(0));
                    z = (IntExpr) ictx.mkSub(z, one);
                    int temp = 2, result = 1;
                    for (int j = 0; j < 30; ++j) {
                        IntExpr y1 = ictx.mkInt(temp);
                        BoolExpr y2 = ictx.mkEq(myLog.apply(y1, ictx.mkInt(2)), ictx.mkInt(result));
                        pat = ictx.mkAnd(pat, y2);
                        temp *= 2;
                        result++;
                    }
                }
            } catch (Z3Exception e) {
                continue;
            }
        }

        return pat;
    }

    public static BoolExpr addAxiomsEXPSubTree(Model m, Expr i, Entry x, BoolExpr t1, BoolExpr b1, Boolean b) {
        BoolExpr pat = ictx.mkTrue();
        IntExpr z = null;
        IntExpr z1 = null;

        for (FuncDecl c : m.getConstDecls()) {
            try {
                if (m.getConstInterp(c).equals(i)) {
                    if (c.getName().toString().startsWith("C")) continue;
                    pat = ictx.mkAnd(pat, Axiom.groundAxExp0(c.getName(), b));
                    pat = ictx.mkAnd(pat, Axiom.groundAxMul0(c.getName(), b));
                    pat = ictx.mkAnd(pat, Axiom.groundAxMulSucc(ictx.mkIntConst(c.getName()), ictx.mkInt(0), b));
                    pat = ictx.mkAnd(pat, Axiom.groundAxExpSucc(ictx.mkIntConst(c.getName()), ictx.mkInt(0), b));

                    IntExpr value = ictx.mkIntConst(c.getName());
                    IntExpr one = ictx.mkInt(1);
                    IntExpr zero = ictx.mkInt(0);

                    z = (IntExpr) i;

                    for (Expr yi : x.getArgs()) {
                        if (!(i.equals(yi))) z = (IntExpr) yi;
                    }

                    for (FuncDecl c1 : m.getConstDecls()) {
                        try {
                            if (m.getConstInterp(c1).equals(z)) {
                                if (c1.getName().toString().startsWith(("C"))) continue;
                                z1 = ictx.mkIntConst(c1.getName());
                            } else z1 = ictx.mkIntConst(c.getName());
                        } catch (Z3Exception e) {
                            continue;
                        }
                    }

                    BoolExpr boolVar = ictx.mkGt(z, ictx.mkInt(0));
                    z = (IntExpr) ictx.mkSub(z, one);

                    for (IntExpr j = (IntExpr) z; !(j.simplify().equals(ictx.mkInt(0))); j = (IntExpr) ictx.mkSub(j, ictx.mkInt(1)).simplify()) {
                        pat = ictx.mkAnd(pat, Axiom.groundAxExpSucc(ictx.mkIntConst(c.getName()), j, b));
                        z1 = (IntExpr) ictx.mkAdd((IntExpr) z1, ictx.mkInt(1));
                    }

                    z = (IntExpr) i;
                    IntExpr powResult = ictx.mkInt(1);
                    for (int a = 0; a < 6; a++) {
                        powResult = (IntExpr) ictx.mkMul(powResult, z);
                    }

                    z = powResult;

                    for (IntExpr j = (IntExpr) z; !(j.simplify().equals(ictx.mkInt(0))); j = (IntExpr) ictx.mkSub(j, ictx.mkInt(1)).simplify()) {
                        pat = ictx.mkAnd(pat, Axiom.groundAxMulSucc(ictx.mkIntConst(c.getName()), j, b));
                        pat = ictx.mkAnd(pat, Axiom.groundAxMulSymmetry(ictx.mkIntConst(c.getName()), j, b));
                        z1 = (IntExpr) ictx.mkAdd((IntExpr) z1, ictx.mkInt(1));
                    }
                }
            } catch (Z3Exception e) {
                continue;
            }
        }

        return pat;
    }

    public static BoolExpr addInputAxiomsSubTree(BoolExpr pathFormula, Model m, int nodeCounter) {
        BoolExpr res = ictx.mkTrue();
        for (FuncDecl f : m.getFuncDecls()) {
            if (f.getName().toString().equals("MUL")) {
                FuncInterp interpretation = m.getFuncInterp(f);
                for (Entry x : interpretation.getEntries()) {

                    Symbol c = getSymbolName(m, x.getValue());
                    if (c != null && !c.toString().startsWith("i0") &&
                            c.toString().startsWith("$i0") && !c.toString().startsWith("i11") &&
                            !(pathFormula.toString().contains("MUL (MUL"))) {
                        try {
                            res = ictx.mkNot(
                                    ictx.mkLt(
                                            ictx.mkIntConst("C" + nodeCounter), ictx.mkMul(ictx.mkIntConst("i0"), ictx.mkInt(1))));

                            return res;
                        } catch (Z3Exception e) {
                            continue;
                        }
                    } else if (c != null && c.toString().contains("main") &&
                            !pathFormula.toString().contains("MUL (MUL")) {
                        res = ictx.mkNot(
                                ictx.mkLt(
                                        ictx.mkIntConst("C" + nodeCounter), ictx.mkMul(ictx.mkIntConst("i0"), ictx.mkInt(1))));
                        return res;
                    } else return pathFormula;
                }
            }
        }
        return res;
    }

    public static BoolExpr addAxiomsMULSubTree(BoolExpr pathFormula, Model m, Expr i, Entry x, BoolExpr t1, BoolExpr b1, Boolean b) {
        BoolExpr pat = ictx.mkTrue();
        IntExpr z = null;
        IntExpr z1 = null;

        for (FuncDecl c : m.getConstDecls()) {
            try {
                if (m.getConstInterp(c).equals(i)) {
                    if (c.getName().toString().startsWith("C")) continue;
                    pat = ictx.mkAnd(pat, Axiom.groundAxMul0(c.getName(), b));
                    pat = ictx.mkAnd(pat, Axiom.groundAxMulSymmetry(ictx.mkIntConst(c.getName()), ictx.mkInt(0), b));
                    pat = ictx.mkAnd(pat, Axiom.groundAxMulSucc(ictx.mkIntConst(c.getName()), ictx.mkInt(0), b));
                    pat = ictx.mkAnd(pat, Axiom.groundAxMulSymmetry(ictx.mkIntConst(c.getName()), ictx.mkInt(1), b));

                    IntExpr one = ictx.mkInt(1);

                    z = (IntExpr) i;

                    for (Expr yi : x.getArgs()) {
                        if (!(i.equals(yi))) z = (IntExpr) yi;
                    }

                    for (FuncDecl c1 : m.getConstDecls()) {
                        try {
                            if (m.getConstInterp(c1).equals(z)) {
                                if (c1.getName().toString().startsWith(("C"))) continue;
                                z1 = ictx.mkIntConst(c1.getName());
                            } else z1 = ictx.mkIntConst(c.getName());
                        } catch (Z3Exception e) {
                            continue;
                        }
                    }
                    BoolExpr boolVar = ictx.mkGt(z, ictx.mkInt(0));

                    z = (IntExpr) ictx.mkSub(z, one);
                    for (IntExpr j = (IntExpr) z; !(j.simplify().equals(ictx.mkInt(0))); j = (IntExpr) ictx.mkSub(j, ictx.mkInt(1)).simplify()) {
                        pat = ictx.mkAnd(pat, Axiom.groundAxMulSucc(ictx.mkIntConst(c.getName()), j, b));
                        pat = ictx.mkAnd(pat, Axiom.groundAxMulSymmetry(ictx.mkIntConst(c.getName()), j, b));
                        z1 = (IntExpr) ictx.mkAdd((IntExpr) z1, ictx.mkInt(1));
                    }

                    return pat;
                }
            } catch (Z3Exception e) {
                continue;
            }
        }

        return pat;
    }
}
