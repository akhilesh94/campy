package safetyTestCode.benchmarkTest;

import safetyChecker.ErrorLable;

public class BinarySearch {
  public static void testBinary(int n)
  {
    int c, first, last, middle, search, array[];   
     
    array = new int[n];    
    
    for (c = 0; c < n; c++)
      array[c] = c + 1;
 
    
    search = n;
 
    first  = 0;
    last   = n - 1;
    middle = (first + last)/2;
 
    while( first <= last )
    {
      if ( array[middle] < search )
        first = middle + 1;    
      else if ( array[middle] == search ) 
      {        
        break;
      }
      else
         last = middle - 1;
 
      middle = (first + last)/2;
   }
    
    ErrorLable.Error();
  }
  
}
