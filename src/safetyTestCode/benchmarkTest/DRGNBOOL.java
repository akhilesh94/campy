package safetyTestCode.benchmarkTest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import safetyChecker.ErrorLable;

class DRGNBOOL {

    /**
     * @param args
     * @throws IOException
     * @throws NumberFormatException
     */
    public static void test(int num, int n, int m) throws NumberFormatException,
            IOException {
        // TODO Auto-generated method stub
        BufferedReader bReader = new BufferedReader(new InputStreamReader(
                System.in));
        int i, j, li, ci, ans;
        String str;
        int[] list;
        
        while (num > 0) {
            str = bReader.readLine();
            list = new int[101];            
            
            for (i = 0; i < n; i++) {
                str = bReader.readLine();
                li = Integer.parseInt(str.split(" ")[1]);
                ci = Integer.parseInt(str.split(" ")[0]);
                list[li] += ci;
            }
            for (i = 0; i < m; i++) {
                str = bReader.readLine();
                li = Integer.parseInt(str.split(" ")[1]);
                ci = Integer.parseInt(str.split(" ")[0]);
                list[li] -= ci;
            }
            ans = 0;
            for (i = 0; i <= 100; i++) {
                if (list[i] < 0)
                    ans -= list[i];
            }
            System.out.println(ans);
            num--;
        }
        ErrorLable.Error();
    }

}
