package safetyTestCode.benchmarkTest;


import safetyChecker.ErrorLable;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Test5 {
    public static void main(int testcases, int n) throws Exception{

        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st;

        for (int i = 0 ; i < testcases ; i++){

            st = new StringTokenizer(r.readLine());
            int[] speeds = new int[n];
            for (int j = 0 ; j < n ; j++){
                speeds[j] = Integer.parseInt(st.nextToken());
            }
            int dep = 0;
            int diff = Integer.MAX_VALUE;
            for (int j = 0 ; j < n ; j++){
                for (int k = dep ; k < n ; k++){
                    if (Math.abs(speeds[j]-speeds[k]) < diff && k != j){
                        diff = Math.abs(speeds[j]-speeds[k]);
                    }
                }
                dep++;
            }
            System.out.println(diff);
        }
        ErrorLable.Error();
    }
}
