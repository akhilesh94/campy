package safetyTestCode.benchmarkTest;


import safetyChecker.ErrorLable;

public class Test0 {
    static int test1(int n){
        int sum=0;
        while(n>0){
            sum=sum+2;
            n--;
        }
        ErrorLable.Error();
        return sum;
    }
    static int test2(int n){
        int sum=0;
        if(n>0){
            sum=2*n;
        }
        return sum;
    }
}
