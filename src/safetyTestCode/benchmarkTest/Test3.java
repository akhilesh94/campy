package safetyTestCode.benchmarkTest;


import safetyChecker.ErrorLable;

import java.util.Scanner;

public class Test3 {
    public static void test (int t, int n) throws java.lang.Exception{
        //Codechef c= new Codechef();
        Scanner sc =new Scanner(System.in);

        for (int i=0; i<t; i++) {
            int min=Integer.MAX_VALUE,ind=-1,
                    max=Integer.MAX_VALUE;
            int[] aij=new int[n];
            for (int j=0; j<n; j++) {
                aij[j]=sc.nextInt();
            }
            for (int j=0; j<n; j++) {
                if (min>aij[j]) {
                    min=aij[j];
                    ind=j;
                }
            }
            for (int j=0; j<n; j++) {
                if (max>aij[j]&&j!=ind) {
                    max=aij[j];
                }
            }
            System.out.println(min+max);
        }

        ErrorLable.Error();
    }
}
