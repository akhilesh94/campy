package safetyChecker;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.FuncDecl;
import com.microsoft.z3.IntExpr;
import com.microsoft.z3.InterpolationContext;
import com.microsoft.z3.Sort;
import com.microsoft.z3.Symbol;

public class Axiom {
    private static InterpolationContext ictx = InterpolationHandler.getItpHandler().getInterpolationContext();

    static Sort iSort = ictx.mkIntSort();
    static Sort bSort = ictx.mkBoolSort();
    static Sort[] domainInt = {iSort, iSort};
    static FuncDecl myMul = ictx.mkFuncDecl("MUL", domainInt, iSort);
    static FuncDecl myExp = ictx.mkFuncDecl("EXP", domainInt, iSort);
    static FuncDecl myLog = ictx.mkFuncDecl("LOG", domainInt, iSort);
    static FuncDecl myAdd = ictx.mkFuncDecl("ADD", domainInt, iSort);
    static FuncDecl myImplies = ictx.mkFuncDecl("IMPLIES", bSort, bSort);
    static IntExpr one = ictx.mkInt(1);
    static IntExpr zero = ictx.mkInt(0);

    public static BoolExpr groundAxMul0(Symbol x, Boolean b) {
        IntExpr x1 = ictx.mkIntConst(x);
        if (b == false) return ictx.mkEq(ictx.mkMul(x1, zero), zero);
        else return (BoolExpr) ictx.mkEq(
                myMul.apply(x1, zero),
                zero
        );
    }

    public static BoolExpr groundAxMulSucc(IntExpr x, IntExpr y, Boolean b) {
        if (b == false) return (BoolExpr) ictx.mkEq(ictx.mkMul(x, ictx.mkAdd(y, one)), ictx.mkAdd(ictx.mkMul(x, y), x));
        else return (BoolExpr) ictx.mkEq(
                myMul.apply(x, ictx.mkAdd(y, one)),
                ictx.mkAdd((IntExpr) myMul.apply(x, y), x)
        );
    }

    public static BoolExpr groundAxMulSymmetry(IntExpr x, IntExpr y, Boolean b) {
        if (b == false) return (BoolExpr) ictx.mkEq(ictx.mkMul(x, y), ictx.mkMul(y, x));
        else return (BoolExpr) ictx.mkEq(
                myMul.apply(x, y),
                myMul.apply(y, x)
        );
    }

    public BoolExpr groundAxAddSucc(Symbol x, Symbol y) {
        IntExpr x1 = ictx.mkIntConst(x);
        IntExpr y1 = ictx.mkIntConst(y);
        return (BoolExpr) ictx.mkEq(
                myAdd.apply(x1, myAdd.apply(y1, one)),
                myAdd.apply(myAdd.apply(x1, y1), one)
        );
    }

    public BoolExpr groundAxAdd0(Symbol x, Symbol y) {
        IntExpr x1 = ictx.mkIntConst(x);
        IntExpr y1 = ictx.mkIntConst(y);
        return (BoolExpr) ictx.mkEq(
                myAdd.apply(x1, zero),
                x1
        );
    }

    public BoolExpr groundSxEqualsSy(Symbol x, Symbol y) {
        IntExpr x1 = ictx.mkIntConst(x);
        IntExpr y1 = ictx.mkIntConst(y);
        return (BoolExpr) myImplies.apply(
                ictx.mkEq(
                        myAdd.apply(x1, one),
                        myAdd.apply(y1, one)
                ),
                ictx.mkEq(
                        x1,
                        y1
                )
        );
    }

    public BoolExpr groundSxNe0(Symbol x) {
        IntExpr x1 = ictx.mkIntConst(x);
        return (BoolExpr) ictx.mkNot(ictx.mkEq(
                zero,
                myAdd.apply(x1, one)
        ));
    }

    public static BoolExpr groundAxExp0(Symbol x, Boolean b) {
        IntExpr x1 = ictx.mkIntConst(x);
        return (BoolExpr) ictx.mkEq(
                myExp.apply(x1, zero),
                one
        );
    }

    public static BoolExpr groundAxExpSucc(IntExpr x, IntExpr y, Boolean b) {
        return (BoolExpr) ictx.mkEq(
                myExp.apply(x, ictx.mkAdd(y, one)),
                myMul.apply((IntExpr) myExp.apply(x, y), x)
        );
    }

    public static BoolExpr groundAxLog0(Symbol x, Boolean b) {
        IntExpr x1 = ictx.mkIntConst(x);
        return (BoolExpr) ictx.mkEq(
                myLog.apply(one, ictx.mkInt(2)),
                zero
        );
    }

    public static BoolExpr groundAxLog2(Symbol x, Boolean b) {
        IntExpr x1 = ictx.mkIntConst(x);
        return (BoolExpr) ictx.mkEq(
                myLog.apply(ictx.mkInt(2), ictx.mkInt(2)),
                one
        );
    }

    public static BoolExpr groundAxLogSucc(IntExpr x, IntExpr y, Boolean b) {
        return (BoolExpr) ictx.mkEq(
                myLog.apply(myMul.apply(x, x), ictx.mkInt(2)),
                myMul.apply(ictx.mkInt(2), myLog.apply(x, ictx.mkInt(2)))
        );
    }

}
