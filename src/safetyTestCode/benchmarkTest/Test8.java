package safetyTestCode.benchmarkTest;

import java.util.Arrays;

import safetyChecker.ErrorLable;

public class Test8 {

    public static void main(String[] args)throws java.io.IOException{

        java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));

        int tests = Integer.parseInt(reader.readLine());
        for(int i = 0; i < tests; i += 1){

            String[] matrixDimensions = reader.readLine().split(" ");
            int rows = Integer.parseInt(matrixDimensions[0]);
            int columns = Integer.parseInt(matrixDimensions[1]);
            char[][] matrix = new char[rows][columns];
            //add chars to matrix
            for(int y = 0; y < rows; y += 1){
                matrix[y] = reader.readLine().toCharArray();
            }//end adding to matrix

            boolean spoonFound = searchMatrix(matrix, "spoon", matrix.length);
            if(spoonFound)
                System.out.print("\r\nThere is a spoon!");
            else
                System.out.print("\r\nThere is indeed no spoon!");

            //          for(int y = 0; y < rows; y += 1){ //print matrix
            //              System.out.println();
            //              for(int x = 0; x < columns; x += 1){
            //                  System.out.print(matrix[y][x]);
            //              }
            //          }
        }//end tests

    }//end main

    public static boolean searchMatrix(char[][] matrix, String s, int n){
        int wordLength = s.length();
        
        n = matrix.length;

        for(int i = 0; i < n; i += 1){
            for(int j = 0; j < n; j += 1){
                //check current index column cells to right
                if(j <= n - wordLength){
                    for(int x = j, count = 0; x < n; x += 1){
                      /*
                        if(Character.toLowerCase(matrix[i][x]) == s.charAt(count))
                            count += 1;
                        else
                            break;
                        if(count == wordLength){
                            //          System.out.println("spoon found at i="+i+" j="+j+" to row="+i+" col="+x + " matrix["+i+"].length-wordLength="+(matrix[i].length-wordLength));
                            return true;
                        }
                        */
                    }
                }//end check -> cells
                //check current index row cells below
                if(i <= n - wordLength){
                    for(int y = i, count = 0; y < n; y += 1){
                      /*
                        if(Character.toLowerCase(matrix[y][j]) == s.charAt(count))
                            count += 1;
                        else
                            break;
                            */
                        if(count == wordLength){
                            //          System.out.println("spoon found at i="+i+" j="+j+" to row="+y+" col="+j + " matrix.length-wordLength="+(matrix.length-wordLength));
                            return true;
                        }
                    }
                }//end check V cells
            }
        }
        ErrorLable.Error();
        return false;
    }
}

