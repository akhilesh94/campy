package safetyChecker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Expr;
import com.microsoft.z3.FuncDecl;
import com.microsoft.z3.InterpolationContext;
import com.microsoft.z3.Log;
import com.microsoft.z3.Sort;

import soot.ArrayType;
import soot.IntegerType;
import soot.Local;
import soot.PrimType;
import soot.RefLikeType;
import soot.RefType;
import soot.SootField;
import soot.Type;
import soot.Unit;
import soot.Value;
import soot.VoidType;
import soot.jimple.AddExpr;
import soot.jimple.AnyNewExpr;
import soot.jimple.ArrayRef;
import soot.jimple.AssignStmt;
import soot.jimple.BinopExpr;
import soot.jimple.CastExpr;
import soot.jimple.Constant;
import soot.jimple.DivExpr;
import soot.jimple.EqExpr;
import soot.jimple.FieldRef;
import soot.jimple.GeExpr;
import soot.jimple.GotoStmt;
import soot.jimple.GtExpr;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.InstanceFieldRef;
import soot.jimple.IntConstant;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.LeExpr;
import soot.jimple.LtExpr;
import soot.jimple.MulExpr;
import soot.jimple.NeExpr;
import soot.jimple.NewArrayExpr;
import soot.jimple.NewExpr;
import soot.jimple.NewMultiArrayExpr;
import soot.jimple.NullConstant;
import soot.jimple.StaticFieldRef;
import soot.jimple.StringConstant;
import soot.jimple.SubExpr;
import soot.jimple.VirtualInvokeExpr;
import soot.jimple.internal.JNewArrayExpr;
import soot.jimple.internal.JNewExpr;
import soot.jimple.internal.JimpleLocal;
import soot.shimple.PhiExpr;
import soot.toolkits.scalar.ValueUnitPair;

public class Z3ScriptHandler {

    private static InterpolationContext ictx;
    private Map<String, Sort> newSortMap = new HashMap<String, Sort>();
    private Map<String, NewSort> sortId = new HashMap<String, NewSort>();
    private Map<String, Expr> global = new HashMap<String, Expr>();
    private Map<String, Expr> localMap = new HashMap<String, Expr>();
    private Map<String, Integer> arrayNameMap = new HashMap<String, Integer>();
    private Map<String, Integer> realArraySize = new HashMap<String, Integer>();
    private Map<String, Integer> maxArraySize = new HashMap<String, Integer>();
    private Map<String, String> substitute = new HashMap<String, String>();
    private Map<String, Sort> substituteSort = new HashMap<String, Sort>();
    private Stack<Expr> parameters = new Stack<Expr>();
    private Z3ArrayHandler arrayHandler = new Z3ArrayHandler();
    private Z3ObjectFieldHandler objFieldHandler = new Z3ObjectFieldHandler();
    private Z3JavaMathHandler z3MathHandler = new Z3JavaMathHandler();
    private Vertex errorPathRoot;
    private Edge currentEdge;

    public static BoolExpr patZ3Script;

    public Z3ScriptHandler(InterpolationContext ictx) {
        this.ictx = ictx;
        patZ3Script = ictx.mkTrue();
    }

    public void convertPathtoZ3Script(Vertex v, Boolean b) {
        errorPathRoot = v;
        boolean isError = false;
        while (v != null) {
            if (isError) break;
            createZ3Script(v.getOutgoingEdge(), b);
            v = v.getNextVertex();
            if (v.getOutgoingEdge().isErrorEdge())
                isError = true;
        }
    }

    public boolean createZ3Script(Edge e, Boolean b) {
        boolean converted = false;
        currentEdge = e;
        if (e.isErrorEdge()) converted = convertErrorEdge(e);
        Unit stmt = e.getUnit();
        if (stmt instanceof IfStmt) converted = this.convertIfStmt(e, b);
        if (stmt instanceof GotoStmt) converted = this.convertGotoStmt(e);
        if (stmt instanceof AssignStmt) converted = this.convertAssignStmtEdge(e, b);
        if (stmt instanceof IdentityStmt) converted = this.convertIdentityStmt(e, b);
        if (e.isSinkEdge()) converted = convertSinkInvoke2Z3(e);
        if (e.isArrayCopyEdge()) converted = convertArrayCopy(e, b);
        if (stmt instanceof InvokeStmt && !e.isSubFunction()) converted = convertNotSubFuntionInvoke(e);

        if (!converted) {
            System.exit(0);
        }
        return converted;
    }

    private boolean convertNotSubFuntionInvoke(Edge edge) {
        edge.setZ3Expr(this.ictx.mkTrue());
        return true;
    }

    private boolean convertArrayCopy(Edge edge, Boolean b) {
        BoolExpr arrayCopyExpr = arrayHandler.z3ArrayCopy(edge, this, b);
        edge.setZ3Expr(arrayCopyExpr);
        if (arrayCopyExpr != null)
            return true;
        return false;
    }

    private boolean convertErrorEdge(Edge edge) {
        InvokeStmt errorInvoke = (InvokeStmt) edge.getUnit();
        return true;
    }

    private boolean convertIfStmt(Edge edge, Boolean b) {
        IfStmt ifStmt = (IfStmt) edge.getUnit();
        Value value = ifStmt.getCondition();
        BoolExpr condition = (BoolExpr) convertValue(value, false, edge, edge.getSource().getDistance(), b);
        Edge nextEdge = edge.getTarget().getOutgoingEdge();

        Unit currentUnit = edge.getUnit();
        LogUtils.detailln("currentUnit=" + currentUnit);
        Unit nextUnit = nextEdge.getUnit();
        LogUtils.detailln("nextUnit=" + nextUnit);
        Unit targetUnit = ifStmt.getTarget();
        LogUtils.detailln("targetUnit = " + targetUnit);

        if (targetUnit.equals(nextUnit))
            edge.setZ3Expr(condition);
        else
            edge.setZ3Expr(this.ictx.mkNot(condition));
        LogUtils.detailln(edge.getUnit() + "=" + edge.getZ3Expr());

        return true;
    }

    private boolean convertIdentityStmt(Edge edge, Boolean b) {
        LogUtils.detailln("Z3ScriptHandler.convertIdentityStmt=" + edge);
        IdentityStmt iStmt = (IdentityStmt) edge.getUnit();
        Value left = iStmt.getLeftOp();
        Expr leftZ3 = convertValue(left, true, edge, edge.getSource().getDistance(), b);

        if (parameters.isEmpty()) {
            Type t = left.getType();
            if (t instanceof RefType) {
                RefType rType = (RefType) t;
                NewExpr right = new JNewExpr(rType);
                Expr rightZ3 = convertValue(right, false, edge, 0, b);
                Type leftType = left.getType();
                BoolExpr expr = convertAssignStmt(rightZ3, leftZ3, leftType, left, edge.getSource().getDistance(), b);

                edge.setZ3Expr(expr);
                if (expr == null)
                    return false;
                return true;
            } else {
                edge.setZ3Expr(this.ictx.mkTrue());
                return true;
            }
        } else {
            LogUtils.fatalln("Z3ScriptHandler.convertIdentityStmt needs to be handled");
            return false;
        }
    }

    private boolean convertAssignStmtEdge(Edge edge, Boolean b) {
        LogUtils.detailln("Z3ScriptHandler.convertAssignStmtEdge=" + edge);
        AssignStmt aStmt = (AssignStmt) edge.getUnit();
        Value left = aStmt.getLeftOp();
        Value right = aStmt.getRightOp();

        if (left.getType() instanceof RefType && right instanceof VirtualInvokeExpr) {
            right = new JNewExpr((RefType) left.getType());
        } else if (right.toString().contains("java.lang.String[] split(java.lang.String)")) {
            right = new JNewArrayExpr(RefType.v("java.lang.String"), IntConstant.v(0));
        }

        Type leftType = left.getType();
        Expr rightZ3 = null;

        if (edge.isSubFunction() && !(((InvokeExpr) right).getMethod().getReturnType() instanceof VoidType)) {
            rightZ3 = this.ictx.mkIntConst("return_" + this.getRealArraySize("return_"));
        } else if (right instanceof InvokeExpr && !edge.isSubFunction()) {
            if (this.z3MathHandler.isJavaMathLibrary(right))
                rightZ3 = this.z3MathHandler.createMathEquality(right, this, edge, b);
            else
                rightZ3 = this.ictx.mkIntConst("nonSubFunction_" + this.getRealArraySize("nonSubFunction_"));
        } else {
            rightZ3 = convertValue(right, false, edge, edge.getSource().getDistance(), b);
        }
        LogUtils.detailln("rightZ3=" + rightZ3);

        Expr leftZ3 = this.convertValue(left, true, edge, edge.getSource().getDistance(), b);
        LogUtils.detailln("leftZ3=" + leftZ3);

        BoolExpr eq = null;

        if (z3MathHandler.isModulusInstruction(right))
            eq = z3MathHandler.createModuleExpr(leftZ3, right, this, edge, b);
        else
            eq = convertAssignStmt(rightZ3, leftZ3, leftType, left, edge.getSource().getDistance(), b);

        if (right instanceof AnyNewExpr) {
            if (right instanceof NewArrayExpr) {
                BoolExpr realArray = arrayHandler.newArrayExpr(rightZ3, right.getType(), this);
                LogUtils.detailln("realArray=" + realArray);
                LogUtils.detailln("eq=" + eq);
                BoolExpr arrayExpr = this.ictx.mkAnd(eq, realArray);

                edge.setZ3Expr(arrayExpr);

            } else if (right instanceof NewMultiArrayExpr) {
                BoolExpr realMulArray = arrayHandler.newMultiArrayExpr((NewMultiArrayExpr) right, right.getType(), this, rightZ3);
                BoolExpr arrayExpr = this.ictx.mkAnd(eq, realMulArray);
                edge.setZ3Expr(arrayExpr);
            } else if (right instanceof NewExpr) {
                edge.setZ3Expr(eq);
            }
        } else if (edge.isSubFunction()) {
            LogUtils.warningln("subfunction is nt complete yet");
            for (Value parameterValue : ((InvokeExpr) right).getArgs()) {
                Expr parameterExpr = this.localMap.get(parameterValue.toString());

                if (parameterExpr == null) {
                    LogUtils.fatalln(("paremeter is null, cannot find the Z3 value"));
                    System.exit(0);
                }
                edge.addParameter(parameterExpr);
                try {
                    if (!edge.getProgramTree().getNewReturnPath())
                        LogUtils.warningln("New return path cannot be found : " + edge);
                } catch (Exception exp) {
                    throw new RuntimeException(exp);
                }
            }

            for (Entry<String, Expr> ee : localMap.entrySet())
                LogUtils.warningln(ee.getKey() + "--" + ee.getValue());
        } else {
            edge.setZ3Expr(eq);
            LogUtils.detailln("eq2=" + eq);
        }
        if (eq == null)
            return false;
        return true;

    }

    private boolean convertGotoStmt(Edge e) {
        e.setZ3Expr(this.ictx.mkTrue());
        return true;
    }

    private boolean convertSinkInvoke2Z3(Edge e) {
        String sign = UnitController.getMethodSignature(e.getUnit());
        Value leakCandidate = null;
        for (String sinkSignature : UnitController.sinkSignatureDB)
            if (sign.contains(sinkSignature))
                leakCandidate = ((InvokeStmt) e.getUnit()).getInvokeExpr().getArg(UnitController.sensitiveParameterMap.get(sinkSignature));

        LogUtils.infoln("Unit : " + e.getUnit());
        LogUtils.infoln("leakCandidate : " + leakCandidate);
        return false;
    }

    protected Expr convertValue(Value value, boolean assignLeft, Edge edge, int nodeIndex, Boolean b) {

        Type type = value.getType();
        if (type instanceof PrimType) {
            return convertPrimitiveValue(value, assignLeft, edge, nodeIndex, b);
        }
        if (type instanceof RefLikeType) {
            return convertRefLikeValue(value, assignLeft, edge, nodeIndex, b);
        }
        LogUtils.fatalln("not a primtype or a refliketype");
        return null;
    }

    private Expr convertPrimitiveValue(Value value, boolean assignLeft, Edge edge, int nodeIndex, Boolean b) {
        LogUtils.detailln("Z3ScriptHandler.convertPrimitiveValue=" + edge);
        if (value instanceof Local) {
            Local local = (Local) value;
            String oldName = local.getName();

            Expr old = ictx.mkIntConst(oldName);
            Expr oldEqualsNew = null;
            if (assignLeft) {
                Type type = value.getType();
                String newName = oldName + getNameSuffix(edge);
                Expr leftExpr = null;
                if (type instanceof IntegerType) {
                    leftExpr = ictx.mkIntConst(newName);
                    patZ3Script = ictx.mkAnd(patZ3Script, ictx.mkEq(old, leftExpr));

                    oldEqualsNew = ictx.mkEq(leftExpr, old);
                    this.substitute.put(newName, oldName);
                    this.substituteSort.put(newName, ictx.mkIntSort());
                }
                localMap.put(oldName, leftExpr);
                return leftExpr;
            } else
                return localMap.get(oldName);
        }
        if (value instanceof BinopExpr) {
            return this.convertBoolExpr((BinopExpr) value, edge, nodeIndex, b);
        }
        if (value instanceof Constant) {
            Constant constant = (Constant) value;
            if (constant instanceof IntConstant) {
                IntConstant intConstant = (IntConstant) constant;
                int intValue = intConstant.value;
                Expr exprValue = ictx.mkInt(intValue);
                return exprValue;
            }
        }
        if (value instanceof PhiExpr) {
            PhiExpr phiExpr = (PhiExpr) value;
            List<ValueUnitPair> pairList = phiExpr.getArgs();

            Vertex vertex = edge.getSource();
            Edge resultEdge = null;
            Value resultValue = null;
            boolean shortestResultFound = false;

            for (ValueUnitPair pair : pairList) {

                Value valuePair = pair.getValue();
                LogUtils.detailln("valuePair=" + valuePair);
                Unit unitPair = pair.getUnit();
                LogUtils.detailln("unitPair=" + unitPair);

                Vertex phiEqualityVertex = errorPathRoot;
                while (phiEqualityVertex != edge.getSource()) {
                    Unit phiEqualityUnit = phiEqualityVertex.getOutgoingEdge().getUnit();
                    if (phiEqualityUnit.equals(unitPair)) {
                        if (resultEdge == null) {
                            resultEdge = phiEqualityVertex.getOutgoingEdge();
                            resultValue = valuePair;
                        } else if (phiEqualityVertex.getDistance() < resultEdge.getSource().getDistance()) {
                            resultEdge = phiEqualityVertex.getOutgoingEdge();
                            resultValue = valuePair;
                        }
                        LogUtils.detailln("phiEqualityUnit=" + phiEqualityUnit + "-- Dist-" + phiEqualityVertex.getDistance());
                        LogUtils.detailln("resultunit=" + resultEdge + " -- Dis=" + resultEdge.getSource().getDistance());
                    }
                    phiEqualityVertex = phiEqualityVertex.getNextVertex();
                }

            }

            Expr resultExpr = convertValue(resultValue, false, edge, edge.getSource().getDistance(), b);
            if (resultExpr == null)
                resultExpr = this.ictx.mkInt(0);

            LogUtils.detailln("resultExpr=" + resultExpr);
            return resultExpr;
        }
        if (value instanceof InvokeExpr) {
            if (edge.isSubFunction()) {
                LogUtils.warningln(value);

            }
        }
        if (value instanceof ArrayRef) {
            ArrayRef arrayRef = (ArrayRef) value;
            return arrayHandler.z3ArrayRef(arrayRef, this, edge, b);

        }
        if (UnitController.isArraysEqualsInvoke(value)) {
            Expr expr = arrayHandler.z3ArraysEqual(value, this, edge, b);
            return expr;
        }

        if (value instanceof InstanceFieldRef) {
            InstanceFieldRef lclStmt = (InstanceFieldRef) value;

            Expr result = this.z3ObjectField(lclStmt, assignLeft, edge, b);
            return result;
        }


        LogUtils.fatalln("returning null");
        LogUtils.fatalln("Vertex=" + edge.getSource() + "---Edge=" + edge);
        LogUtils.fatalln("Z3ScriptHandler.convertPrimitiveValue");
        return null;
    }

    private Expr convertRefLikeValue(Value value, boolean assignLeft, Edge edge, int nodeIndex, Boolean b) {
        LogUtils.detailln("Z3ScriptHandler.convertRefLikeValue=" + edge);
        if (value instanceof PhiExpr) {
            LogUtils.fatalln("FATAL: PhiExpr is not supported yet!");
            System.exit(0);
        }
        if (value instanceof Local) {
            Type type = value.getType();
            Local local = (Local) value;
            if (type instanceof RefType) {
                return createZ3Object(local, assignLeft, edge);
            }
            if (type instanceof ArrayType) {
                Expr result = this.arrayHandler.z3Local(local, assignLeft, nodeIndex, this);
                return result;
            }
        }
        if (value instanceof AnyNewExpr) {
            Expr result = convertAnyNewExpr((AnyNewExpr) value, edge);
            return result;
        }
        if (value instanceof StringConstant) {
            LogUtils.fatalln("FATAL: StringConstant. is not supported yet!");
            System.exit(0);
        }
        if (value instanceof ArrayRef) {
            ArrayRef arrayRef = (ArrayRef) value;
            return arrayHandler.z3ArrayRef(arrayRef, this, edge, b);
        }
        if (value instanceof InstanceFieldRef) {
            LogUtils.fatalln("FATAL: InstanceFieldRef is not supported yet!");
            System.exit(0);
        }
        if (value instanceof CastExpr) {
            LogUtils.fatalln("FATAL: CastExpr is not supported yet!");
            System.exit(0);
        }
        if (value instanceof StaticFieldRef) {
            StaticFieldRef sfRef = (StaticFieldRef) value;
            SootField field = sfRef.getField();
            String name = field.getName();
            Local newLocal = new JimpleLocal(name, field.getType());
            return objFieldHandler.handleStaticFieldRef(newLocal, assignLeft, this);
        }
        if (value instanceof NullConstant) {
            LogUtils.fatalln("FATAL: NullConstant is not supported yet!");
            System.exit(0);
        }

        LogUtils.fatalln("FATAL: Conversion cannot be done");
        LogUtils.fatalln("FATAL: Unit : " + edge.getUnit() + " - Value : " + value);
        LogUtils.fatalln("Z3ScriptHandler.convertRefLikeValue");
        return null;
    }

    private Expr createZ3Object(Local v, boolean IfAssignLeft, Edge e) {
        Type type = v.getType();
        String TypeName = type.toString();
        Sort newSort = null;
//		Map<String, NewSort> sortId = theCoverter.getSortId();
        if (newSortMap.containsKey(TypeName)) {
            newSort = newSortMap.get(TypeName);
        } else {
            newSort = ictx.mkUninterpretedSort(ictx.mkSymbol(TypeName));
            newSortMap.put(TypeName, newSort);
        }
        if (!global.containsKey(TypeName)) {
            Sort newArraySort = ictx.mkArraySort(ictx.getIntSort(), ictx.getIntSort());
            String arrayName = this.getGlobalName(TypeName);
            Expr newArray = ictx.mkConst(arrayName, newArraySort);
            global.put(TypeName, newArray); //theCoverter.updateGlobal(TypeName, newArray);
            substitute.put(TypeName, arrayName);
            NewSort s = new NewSort(newSort, ictx);
            sortId.put(TypeName, s);
        }
        if (IfAssignLeft) {
            String valueName = v.getName() + e.getProgramTree().getProgramDefinition();
            Expr a = ictx.mkConst(valueName, newSortMap.get(TypeName));
            NewSort s2 = sortId.get(TypeName);
            if (s2.ifHasExpr(a)) {
                return a;
            } else {
                s2.creatNewOject(a);
                return a;
            }
        } else {
            ArrayExpr oldArray = (ArrayExpr) global.get(TypeName);
            NewSort s2 = sortId.get(TypeName);
            String valueName = v.getName() + e.getProgramTree().getProgramDefinition();
            Expr a = ictx.mkConst(valueName, newSort);// newSortMap.get(TypeName));
            Expr result = this.ictx.mkSelect(oldArray, s2.getId(a));
            return result;
        }
    }

    public String getGlobalName(String name) {
        String globalName = null;
        int index = 1;
        if (arrayNameMap.containsKey(name)) {
            index = arrayNameMap.get(name);
            index++;
        }

        globalName = "Global_" + name + "_" + index;
        arrayNameMap.put(name, index);
        return globalName;
    }

    private BoolExpr convertAssignStmt(Expr rightZ3, Expr leftZ3, Type leftType, Value left, int distance, Boolean b) {
        if ((leftType instanceof PrimType) && (left instanceof Local)) {
            BoolExpr leftEqRight = this.ictx.mkEq(leftZ3, rightZ3);
            return leftEqRight;
        }
        if ((leftType instanceof ArrayType) && (left instanceof Local)) {
            String typeName = leftType.toString();
            String virtualName = typeName;
            String newName = virtualName + this.getNameSuffix();

            ArrayExpr latestArray = (ArrayExpr) this.localMap.get(virtualName);
            LogUtils.detailln("latestArray=" + latestArray);
            ArrayExpr newArray = (ArrayExpr) this.ictx.mkConst(newName, latestArray.getSort());
            LogUtils.detailln("newArray=" + newArray);
            this.substitute.put(newName, virtualName);
            this.substituteSort.put(newName, newArray.getSort());
            this.localMap.put(virtualName, newArray);

            String sortName = typeName + this.getArraySortSuffix();
            NewSort s = sortId.get(sortName);

            Expr afterStore = ictx.mkStore((ArrayExpr) latestArray, s.getId(leftZ3), rightZ3);
            LogUtils.detailln("afterStore=" + afterStore);
            BoolExpr newArrayEqOldArray = ictx.mkEq(newArray, afterStore);
            return newArrayEqOldArray;
        }
        if (left instanceof ArrayRef) {
            ArrayRef leftRef = (ArrayRef) left;
            BoolExpr result = arrayHandler.updateArrayRef(leftRef, this, rightZ3, currentEdge, b);
            return result;
        } else {
            String oldName = getArrayName(left);
            String newName = getGlobalName(oldName);
            Expr latestArray = global.get(oldName);
            Expr newArray = ictx.mkConst(newName, latestArray.getSort());
            substitute.put(newName, oldName);
            global.put(oldName, newArray);
            NewSort s = sortId.get(oldName);
            Expr afterStore = null;

            if ((left instanceof FieldRef) && (!(left instanceof StaticFieldRef)))
                afterStore = ictx.mkStore((ArrayExpr) latestArray, leftZ3, rightZ3);
            else
                afterStore = ictx.mkStore((ArrayExpr) latestArray, s.getId(leftZ3), rightZ3);

            LogUtils.detailln("afterStore=" + afterStore);
            BoolExpr newArrayEqOldArray = ictx.mkEq(newArray, afterStore);
            return newArrayEqOldArray;
        }
    }

    private Expr convertAnyNewExpr(AnyNewExpr ane, Edge e) {
        LogUtils.detailln("Z3ScriptHandler.convertAnyNewExpr");
        if (ane instanceof NewExpr) return convertNewExpr((NewExpr) ane, e);
        if (ane instanceof NewArrayExpr) return arrayHandler.convertNewArrayExpr((NewArrayExpr) ane, e, this);
        if (ane instanceof NewMultiArrayExpr)
            return arrayHandler.convertNewMultiArrayExpr((NewMultiArrayExpr) ane, e, this);
        return null;
    }

    private Expr convertNewExpr(NewExpr ne, Edge e) {
        Type t = ne.getType();
        String typeName = t.toString();
        if (sortId.containsKey(typeName)) {
            NewSort s = sortId.get(typeName);
            return s.getNewObject();
        } else {
            Sort newSort = null;
            if (this.newSortMap.containsKey(typeName)) {
                newSort = this.newSortMap.get(typeName);
            } else {
                newSort = this.ictx.mkUninterpretedSort(typeName);
                newSortMap.put(typeName, newSort);
            }
            NewSort ns = new NewSort(newSort, this.ictx);
            sortId.put(typeName, ns);
            return ns.getNewObject();
        }

    }

    private String getArrayName(Value leftOp) {
        Type t = leftOp.getType();
        if (leftOp instanceof Local) return t.toString();
        else if (leftOp instanceof InstanceFieldRef) {
            InstanceFieldRef ref = (InstanceFieldRef) leftOp;

            SootField field = ref.getField();
            return field.toString();
        }

        throw new RuntimeException();
    }

    private String getNameSuffix(Edge e) {
        return "_" + e.getProgramTree().getProgramDefinition() + "_" + e.getSource().getDistance();
    }

    protected String getNameSuffix() {
        return "_" + currentEdge.getProgramTree().getProgramDefinition() + "_" + currentEdge.getSource().getDistance();
    }

    protected String getArrayNameSuffix() {
        return "_" + currentEdge.getProgramTree().getProgramDefinition();
    }

    protected String getArraySortSuffix() {
        return "_arraySort";
    }

    protected int getRealArraySize(String name) {
        if (this.realArraySize.containsKey(name)) {
            int size = this.realArraySize.get(name);
            this.realArraySize.put(name, ++size);
            return size;
        } else {
            int size = 1;
            this.realArraySize.put(name, size);
            return size;
        }

    }

    private Expr convertBoolExpr(BinopExpr expr, Edge edge, int nodeIndex, Boolean b) {

        if (expr instanceof AddExpr) {
            AddExpr addExpr = (AddExpr) expr;
            Value op1Value = addExpr.getOp1();
            Value op2Value = addExpr.getOp2();

            Expr op1Expr = convertValue(op1Value, false, edge, edge.getSource().getDistance(), b);
            Expr op2Expr = convertValue(op2Value, false, edge, edge.getSource().getDistance(), b);

            return ictx.mkAdd((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }
        if (expr instanceof SubExpr) {
            SubExpr subExpr = (SubExpr) expr;
            Value op1Value = subExpr.getOp1();
            Value op2Value = subExpr.getOp2();

            Expr op1Expr = convertValue(op1Value, false, edge, nodeIndex, b);
            Expr op2Expr = convertValue(op2Value, false, edge, nodeIndex, b);

            return this.ictx.mkSub((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }
        if (expr instanceof MulExpr) {
            MulExpr mullExpr = (MulExpr) expr;
            Value op1Value = mullExpr.getOp1();
            Value op2Value = mullExpr.getOp2();


            Sort iSort = ictx.mkIntSort();
            Sort[] domainInt = {iSort, iSort};

            FuncDecl myMul = ictx.mkFuncDecl("MUL", domainInt, iSort);

            Expr op1Expr = convertValue(op1Value, false, edge, nodeIndex, b);
            Expr op2Expr = convertValue(op2Value, false, edge, nodeIndex, b);

            if (b == false) return ictx.mkMul((ArithExpr) op1Expr, (ArithExpr) op2Expr);
            else {
                if ((op1Value instanceof IntConstant) || (op2Value instanceof IntConstant))
                    return ictx.mkMul((ArithExpr) op1Expr, (ArithExpr) op2Expr);
                else return myMul.apply((ArithExpr) op1Expr, (ArithExpr) op2Expr);
            }
        }
        if (expr instanceof DivExpr) {
            DivExpr divExpr = (DivExpr) expr;
            Value op1Value = divExpr.getOp1();
            Value op2Value = divExpr.getOp2();

            Expr op1Expr = convertValue(op1Value, false, edge, nodeIndex, b);
            Expr op2Expr = convertValue(op2Value, false, edge, nodeIndex, b);

            return this.ictx.mkDiv((ArithExpr) op1Expr, (ArithExpr) op2Expr);

        }
        if (expr instanceof EqExpr) {
            EqExpr eqExpr = (EqExpr) expr;
            Value op1Value = eqExpr.getOp1();
            Value op2Value = eqExpr.getOp2();

            Expr op1Expr = convertValue(op1Value, false, edge, edge.getSource().getDistance(), b);
            Expr op2Expr = convertValue(op2Value, false, edge, edge.getSource().getDistance(), b);

            return ictx.mkEq(op1Expr, op2Expr);
        }
        if (expr instanceof NeExpr) {
            NeExpr neExpr = (NeExpr) expr;
            Value op1Value = neExpr.getOp1();
            Value op2Value = neExpr.getOp2();

            Expr op1Expr = convertValue(op1Value, false, edge, edge.getSource().getDistance(), b);
            Expr op2Expr = convertValue(op2Value, false, edge, edge.getSource().getDistance(), b);

            BoolExpr eqExpr = this.ictx.mkEq(op1Expr, op2Expr);
            return this.ictx.mkNot(eqExpr);

        }
        if (expr instanceof GtExpr) {
            GtExpr gtExpr = (GtExpr) expr;
            Value op1Value = gtExpr.getOp1();
            Value op2Value = gtExpr.getOp2();

            Expr op1Expr = convertValue(op1Value, false, edge, edge.getSource().getDistance(), b);
            Expr op2Expr = convertValue(op2Value, false, edge, edge.getSource().getDistance(), b);

            return this.ictx.mkGt((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }
        if (expr instanceof GeExpr) {
            GeExpr geExpr = (GeExpr) expr;
            Value op1Value = geExpr.getOp1();
            Value op2Value = geExpr.getOp2();

            Expr op1Expr = convertValue(op1Value, false, edge, edge.getSource().getDistance(), b);
            Expr op2Expr = convertValue(op2Value, false, edge, edge.getSource().getDistance(), b);

            return this.ictx.mkGe((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }
        if (expr instanceof LtExpr) {
            LtExpr ltExpr = (LtExpr) expr;
            Value op1Value = ltExpr.getOp1();
            Value op2Value = ltExpr.getOp2();

            Expr op1Expr = convertValue(op1Value, false, edge, edge.getSource().getDistance(), b);
            Expr op2Expr = convertValue(op2Value, false, edge, edge.getSource().getDistance(), b);

            return this.ictx.mkLt((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }
        if (expr instanceof LeExpr) {
            LeExpr leExpr = (LeExpr) expr;
            Value op1Value = leExpr.getOp1();
            Value op2Value = leExpr.getOp2();

            Expr op1Expr = convertValue(op1Value, false, edge, edge.getSource().getDistance(), b);
            Expr op2Expr = convertValue(op2Value, false, edge, edge.getSource().getDistance(), b);

            return this.ictx.mkLe((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }


        // LogUtils.fatalln("Z3ScriptHandler.convertBoolExpr returns null for " + expr);
        return null;
    }

    public Expr z3ObjectField(InstanceFieldRef ref, boolean assignLeft, Edge edge, Boolean b) {
        LogUtils.detailln("z3ObjectField");
        Value base = ref.getBase();
        SootField field = ref.getField();
        Expr baseZ3 = this.convertValue(base, false, edge, edge.getSource().getDistance(), b);
        String fieldName = field.toString();
        if (!this.global.containsKey(fieldName)) {
            Sort newArraySort = this.ictx.mkArraySort(this.ictx.getIntSort(), this.ictx.getIntSort());
            String arrayName = this.getGlobalName(fieldName);
            Expr newArray = this.ictx.mkConst(arrayName, newArraySort);
            this.global.put(fieldName, newArray);
        }

        if (assignLeft) {
            return baseZ3;
        } else {
            ArrayExpr oldArray = (ArrayExpr) this.global.get(fieldName);
            Expr result = this.ictx.mkSelect(oldArray, baseZ3);
            return result;
        }
    }

    public InterpolationContext getIctx() {
        return this.ictx;
    }

    public Map<String, Expr> getGlobal() {
        return this.global;
    }

    public Map<String, String> getSubstitute() {
        return this.substitute;
    }

    public Map<String, Sort> getSubstituteSort() {
        return this.substituteSort;
    }

    public Map<String, Integer> getArrayNameMap() {
        return this.arrayNameMap;
    }

    public Map<String, Expr> getLocalMap() {
        return this.localMap;
    }

    public Map<String, NewSort> getSortId() {
        return this.sortId;
    }

    public Map<String, Sort> getNewSortMap() {
        return this.newSortMap;
    }

    public Map<String, Integer> getMaxArraySize() {
        return this.maxArraySize;
    }

}
