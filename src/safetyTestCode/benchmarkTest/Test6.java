package safetyTestCode.benchmarkTest;


import safetyChecker.ErrorLable;

import java.io.InputStreamReader;
import java.util.Scanner;

public class Test6 {
    private void solve(int n, int x) {
        Scanner sc = new Scanner(new InputStreamReader(System.in));
        int[] a = new int[x];
        // for (int i = 0; i < x; i++) a[i] = sc.nextInt();
        for (int i = 0; i < x; i++) {
            int s = 1;
            for (int j = 0; j < x - i; j++) {
                while(a[j + i]>0){
                    if(a[j + i]%2==1)
                        j++;
                    a[j + i]=a[j + i]/2;
                }
                if (a[j] > a[i]) s++;
            }

            if (i < x - 1) System.out.print(" ");
            else System.out.println(s);
        }
        ErrorLable.Error();
    }
}
