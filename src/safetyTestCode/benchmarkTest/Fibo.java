package safetyTestCode.benchmarkTest;

import safetyChecker.ErrorLable;

public class Fibo {

  public static void test(int n, int a, int b) {
    int next = 0;
    for(int i = 2; i < n; i++) {
      next = (b * b) + (a);
      a = b;
      b = next;
    }
    
    ErrorLable.Error();
  }
}
