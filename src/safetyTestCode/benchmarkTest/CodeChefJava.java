package safetyTestCode.benchmarkTest;

import java.util.Scanner;
import safetyChecker.ErrorLable;
class CodeChefJava { 
    public static void test(String s, int t, int n)
    {
         n = s.length();
         Scanner sc = new Scanner(System.in);         
         for(int i=0;i<t;i++)
         {            
            s = sc.next();
            int bal = 0;
            int max_bal = 0;
            for(int j=0;j<s.length();j++)
            {
                if(s.charAt(j)=='(')
                {
                    ++bal;
                }
                if(s.charAt(j)==')')
                {
                    --bal;
                    max_bal = Math.max(max_bal, bal);
                }
            }
//            System.out.println(max_bal);
            int count1 = max_bal +1;
            int count2 = max_bal +1;
            while(count1!=0)
            {
                System.out.print('(');
                count1--;
            }
            while(count2!=0)
            {
                System.out.print(')');
                count2--;
            }
            System.out.println();
            
         }
         ErrorLable.Error();
    }
} 
