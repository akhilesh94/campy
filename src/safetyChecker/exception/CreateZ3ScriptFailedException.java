package safetyChecker.exception;

public class CreateZ3ScriptFailedException extends Exception {
	
	public static final long serialVersionUID = 1L;

	public CreateZ3ScriptFailedException(String message) {
		super(message);
	}

	public String getMessage() {
		return super.getMessage();
	}
}	

